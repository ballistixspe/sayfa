// var toggleClicked = function(e) {
//     e.preventDefault();
//     var $target = $(e.target),
//         href = $($target.attr('href'));
//     $target.toggleClass('is-active');
//     href.toggleClass('is-active');
// };
// $(document).on('click', '*[data-element="toggle"]', toggleClicked);


$('body').on('click', '*[data-element="toggle"]', function(event) {
    event.preventDefault();
    var target = $(this).attr('href');
    $(this).toggleClass('is-active');
	$(target).toggleClass('is-active');
});

$('*[data-element="closeSearch"]').click(function () {
    $('#search-offcanvas').removeClass('is-active');
    $('.navbar-search').removeClass('is-active');
});

$(window).scroll(function () {
    $('#search-offcanvas').removeClass('is-active');
    $('.navbar-search').removeClass('is-active');
});
$.fn.stretchLeft = function() {
    this.each(function(index, el) {
        var stretch = $(this).offset().left;
        $(this).css("margin-left", -stretch);
        $(this).css("padding-left", stretch);
    });
};
$('.navigation-sidebar').find('a').stretchLeft();

$('#menu-main-menu').prepend('<button class="toggle-menu" data-element="toggle" href="#main-navigation"><span>menu</span></button>');

// $('body').on('click', '.menu-item-has-children > a', function(event) {
//
// 	event.preventDefault();
//
// 	$('#menu-main-menu').find('.active').removeClass('active');
//
// 	$(this).next('.sub-menu').toggleClass('active');
// 	return false;
// });

$({property: 0}).animate({property: 105}, {
duration: 4000,
step: function() {
  var _percent = Math.round(this.property);
  $("#progress").css("width",  _percent+"%");
  if(_percent == 105) {
    $("#progress").removeClass("waiting");
    $("#progress").addClass("done");
  }
},
complete: function() {
  //alert("complete");
}
});

$(window).scroll(function () {
    var scrollTop = $(this).scrollTop();
    $('*[data-element="scroll"]').each(function () {
        var topPosScroll = $(this).data('trigger');
        if (scrollTop > topPosScroll) {
            $(this).addClass('is-scrolled');
        } else {
            $(this).removeClass('is-scrolled');
        }
    });
});
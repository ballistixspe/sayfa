$.fn.fullRow = function() {
    this.each(function(index, el) {
        var row = $(this);
        var stretch = row.position().left;
        var type = row.data('stretch-type');
        row.css("margin-left", -stretch);
        row.css("margin-right", -stretch);
        if (type == 'full') {
            row.css("padding-left", stretch);
            row.css("padding-right", stretch);
        }
    });
};

$('.full-row').fullRow();
$( window ).resize(function() {
  $('.full-row').fullRow();
});
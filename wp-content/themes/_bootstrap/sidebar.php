<?php
/**
* Sidebar Template
*
* @package dash
*/ ?>

<?php if ( is_active_sidebar( 'sidebar-widget' ) ) : ?>
	</div>
	<div class="col-sm-3">
		<aside id="sidebar" role="complementary">
	        <?php dynamic_sidebar( 'sidebar-widget' ); ?>
		</aside>
<?php endif; ?>

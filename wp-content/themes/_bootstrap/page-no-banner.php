<?php 
/* Template Name: No Banner 
*
* @package dash
*/ ?>
<?php get_header(); ?>
<section id="content" role="main">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        <section class="page-content">
            <?php the_content(); ?>
        </section>
    </article>
    <?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>

<?php
/**
* Header Template
*
* @package dash
*/ ?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php wp_title( ' | ', true, 'right' ); ?></title>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
<!--   <div id="progress" class="waiting">&nbsp;</div> -->
<!-- <a href="javascript:void(0);" class="back-to-top" data-toggle="tooltip" data-placement="left"  title="Back to Top"><i class="fa fa-arrow-up" aria-hidden="true"></i></a> -->
<div id="page-wrap" class="hfeed">
<div id="search-offcanvas">
<!-- <a data-element="closeSearch">
<span class="close glyphicon glyphicon-remove" aria-hidden="true"></span>
</a> -->
<?php _dash_search_form(''); ?>
</div>
<header id="site-header" data-element="scroll" data-trigger="300">

  <a data-element="toggle" href="#search-offcanvas" class="search navbar-search">SEARCH</a>

<div id="masthead">
<div class="brand">
<?php
if ( has_custom_logo() ) :
the_custom_logo();
else : ?>
<a class="brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( get_bloginfo( 'name' ), '_dash' ); ?>" rel="home">
<?php echo sprintf('<h1 class="site-title">%s</h1>', bloginfo( 'name' ) ); ?>
</a>
<?php endif; ?>
<div class="description">
  <?php echo bloginfo( 'description' ) ?>
  <a href="tel:<?php echo str_replace(" ", "", get_theme_mod("client_phone") ); ?>" class="phone"><?php echo get_theme_mod("client_phone"); ?></a>
</div>
</div>
 <div class="header-widget">

<?php
/**
* Displays a navigation menu
* @param array $args Arguments
*/
$args = array(
'theme_location' => 'main-menu',
'menu' => 'Main Menu',
'container' => 'nav',
'container_class' => '',
'container_id' => 'main-navigation',
'menu_class' => 'menu',
'menu_id' => '',
'echo' => true,
'fallback_cb' => '',
'before' => '',
'after' => '',
'link_before' => '',
'link_after' => '',
'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
'depth' => 2,
'walker' => ''
);
wp_nav_menu( $args );
?>
</div>
</div>
</header>
<main id="site-content">
<div class="container">

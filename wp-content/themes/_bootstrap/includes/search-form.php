<?php

function _dash_search_form( $search_class ) {

	echo '<form role="search" method="get" id="searchform" class="searchform ' . $search_class .'" action="' . esc_url( home_url( '/' ) ) . '" >
	<div class="input-group">
		<input type="search" class="form-control search-field" placeholder="' . esc_attr_x( 'What are you looking for &hellip;', '_dash' ) . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x( 'Search for:', '_dash' ) . '" />
		<span class="input-group-btn">
			<input type="submit" class="search-submit btn btn-default" value="'. esc_attr_x( 'Search', '_dash' ) .'" />
		</span>
    </div>
	</form>';
}

function _dash_search_form_filter( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="searchform" action="' . esc_url( home_url( '/' ) ) . '" >
	<div class="input-group">
		<input type="search" class="form-control search-field" placeholder="' . esc_attr_x( 'Search &hellip;', '_dash' ) . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x( 'Search for:', '_dash' ) . '" />
		<span class="input-group-btn">
			<input type="submit" class="search-submit btn btn-default" value="'. esc_attr_x( 'Search', '_dash' ) .'" />
		</span>
    </div>
	</form>';
	return $form;
}

add_filter( 'get_search_form', '_dash_search_form_filter');
 ?>

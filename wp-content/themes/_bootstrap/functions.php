<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

add_filter( 'the_excerpt', 'remove_shortcodes_in_excerpt', 20 );

function remove_shortcodes_in_excerpt( $content){
    $content = strip_shortcodes($content);
    $tagnames = array('box', 'alert');  // add shortcode tag name [box]content[/box] tagname = box
    $content = do_shortcodes_in_html_tags( $content, true, $tagnames );

    $pattern = get_shortcode_regex( $tagnames );
    $content = preg_replace_callback( "/$pattern/", 'strip_shortcode_tag', $content );
    return $content;
}

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles()
{

  //wp_enqueue_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array(), '2.8.3', true);
  //wp_enqueue_script('fontawesome', 'https://use.fontawesome.com/7d3bcb62c3.js',array(), '1.0.0', TRUE );
  //wp_enqueue_script('typekit', 'https://use.typekit.net/kof4cay.js',array(), '1.0.0', TRUE );

  wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', '', '4.7.0');

  wp_enqueue_style('fancybox', 'https://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css', '', '2.1.5');
  wp_enqueue_script('fancybox', 'https://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js', array('jquery'), '2.1.5', true);
  //wp_enqueue_script('smoothScroll', 'https://cdnjs.cloudflare.com/ajax/libs/smoothscroll/1.4.6/SmoothScroll.min.js', array('jquery'), '1.4.6', true);

  wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/dist/js/bootstrap.min.js', array('jquery'), '3.3.5', true);
  wp_enqueue_style('_bootstrap', get_stylesheet_directory_uri() . '/dist/css/style.min.css', '', '1.2.29');
  wp_enqueue_script('_bootstrap', get_stylesheet_directory_uri() . '/dist/js/scripts.min.js', array('jquery'), '1.2.28', true);

  wp_enqueue_style('style', get_stylesheet_uri());

}

add_action('widgets_init', '_bs_widgets_init');
function _bs_widgets_init()
{
  register_sidebar(array('name' => __('Taxonomy Product', '_dash'), 'id' => 'taxonomy_product_widget', 'before_widget' => '<aside id="%1$s" class="widget %2$s">', 'after_widget' => '</aside>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>'));
}


add_action('wp_head', 'youttheme_font_typekit');
function youttheme_font_typekit() {
  echo '<script src="https://use.typekit.net/kof4cay.js"></script><script>try{Typekit.load({ async: false });}catch(e){}</script>';
}

require_once get_stylesheet_directory() . '/includes/search-form.php';


// function _remove_script_version($src)
// {
//   $parts = explode('?', $src);
//   return $parts[0];
// }
// add_filter('script_loader_src', '_remove_script_version', 15, 1);
// add_filter('style_loader_src', '_remove_script_version', 15, 1);


// function disable_emojicons_tinymce($plugins)
// {
//   if (is_array($plugins)) {
//     return array_diff($plugins, array('wpemoji'));
//   } else {
//     return array();
//   }
// }

// function disable_wp_emojicons()
// {

//   // all actions related to emojis
//   remove_action('admin_print_styles', 'print_emoji_styles');
//   remove_action('wp_head', 'print_emoji_detection_script', 7);
//   remove_action('admin_print_scripts', 'print_emoji_detection_script');
//   remove_action('wp_print_styles', 'print_emoji_styles');
//   remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
//   remove_filter('the_content_feed', 'wp_staticize_emoji');
//   remove_filter('comment_text_rss', 'wp_staticize_emoji');

//   // filter to remove TinyMCE emojis
//   add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');

//   add_filter('emoji_svg_url', '__return_false');
// }
// add_action('init', 'disable_wp_emojicons');

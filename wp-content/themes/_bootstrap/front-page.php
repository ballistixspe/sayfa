<?php
/**
* frontpage Template
*
* @package dash
*/ ?>
<?php get_header(); ?>
<section id="content" role="main">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <?php the_content( ); ?>
    <?php endwhile; endif; ?>
    
</section>
<?php get_footer(); ?>

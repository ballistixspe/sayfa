<?php
/**
* Single Template
*
* @package dash
*/ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php 
		$image = get_field('global_page_header_banner', 'option');
		$featuredImage = $image['url'];
 ?>
<header class="page-header full-row" data-stretch-type="full-stretched" style="background-image: url( <?php echo $featuredImage; ?> );">
  <div class="page-title">
    <div class="container">
    	<?php the_title('<h1 class="the_title">', '</h1>'); ?>
    </div>
  </div>
</header>
<section class="secondary-line full-row" data-stretch-type="full">
  <div class="row">
    <div class="col-sm-12">
      <ol class="breadcrumb">
        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
          <a href="<?php echo home_url();?>" itemprop="url">
            <span itemprop="title">Home</span>
          </a>
        </li>
        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
          <a href="<?php echo get_the_permalink(125); ?>" itemprop="url">
            <span itemprop="title">Products</span>
          </a>
        </li>  
        <li class="current"><?php the_title(); ?></li></ol>
    </div>
  </div>
</section>

<section id="content" role="main" class="col-sm-12">
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <section class="page-content">
      <div class="row">
        <div class="col-sm-4">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
          <a href="<?php echo $image[0]; ?>" id="image-placeholder" class="fancybox"><?php the_post_thumbnail('medium'); ?> </a>
          <div class="clearfix">&nbsp;</div>
              <table>
                <tr>
                  <td><img src=" <?php echo plugins_url('/img/sayfa_logo_dark_small.png', dirname(__FILE__)); ?>" alt=""></td>
                </tr>
              </table>
            </div>
            <div class="col-sm-8">
              <div role="tabpanel">
                  <ul class="nav nav-tabs" role="tablist">
                  <?php if (get_field('description', $post->ID )): ?>
                      <li role="presentation" class="active">
                          <a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a>
                      </li>
                    <?php endif; ?>
                  <?php if (get_field('specification', $post->ID )): ?>
                      <li role="presentation">
                          <a href="#specification" aria-controls="specification" role="tab" data-toggle="tab">Specification</a>
                      </li>
                    <?php endif; ?>
                    <?php if (get_field('pdf', $post->ID )): ?>
                        <li role="presentation">
                            <a href="#literature" aria-controls="literature" role="tab" data-toggle="tab">Literature</a>
                        </li>
                      <?php endif; ?>
                  <?php if (get_field('gallery', $post->ID )): ?>
                      <li role="presentation">
                          <a href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a>
                      </li>
                    <?php endif; ?>
                        <?php if( have_rows('tech_drawings') ):
                      echo '<li role="presentation">
                          <a href="#tech-drawings" aria-controls="tech-drawings" role="tab" data-toggle="tab">Tech Drawings</a></li>';
                    endif; ?>
                  </ul>
                <div class="clearfix">&nbsp;</div>
                <!-- Tab panes -->
                  <div class="tab-content">
                  <?php if (get_field('description', $post->ID )): ?>
                      <div role="tabpanel" class="tab-pane active" id="description">
                           <?php echo wpautop(the_field('description', $post->ID )); ?>
                      </div>
                  <?php endif; ?>
                <?php if (get_field('specification', $post->ID )): ?>
                      <div role="tabpanel" class="tab-pane" id="specification">
                           <?php echo wpautop(the_field('specification', $post->ID )); ?>
                      </div>
                  <?php endif; ?>
                    <?php if( have_rows('pdf') ):
                  echo '<div role="tabpanel" class="tab-pane" id="literature">';
                    echo '<ul>';
                    while ( have_rows('pdf') ) : the_row();
                        $file = get_sub_field('file');
                        if( $file ):
                            echo '<li><a href="' . $file['url'] . '" target="_blank">' . $file['title'] . '</li></a>';
                        endif;
                    endwhile;
                    echo '</ul>';
                    echo '</div>';
                endif; ?>
                <?php
                $images = get_field('gallery', $post->ID);
                  if( $images ): ?>
                    <div role="tabpanel" class="tab-pane" id="gallery">
                        <ul>
                            <?php foreach( $images as $image ): ?>
                                <li>
                                    <a class="gallery-thumbnail" href="#" data-full="<?php echo $image['url']; ?>">
                                         <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                      </div>
                  <?php endif; ?>

                    <?php if( have_rows('tech_drawings') ):
                  echo '<div role="tabpanel" class="tab-pane" id="tech-drawings">';
                    echo '<ul>';
                    while ( have_rows('tech_drawings') ) : the_row();
                        $file = get_sub_field('file');
                        if( $file ):
                            echo '<li><a href="' . $file['url'] . '" target="_blank">' . $file['title'] . '</li></a>';
                        endif;
                    endwhile;
                    echo '</ul>';
                    echo '</div>';
                endif; ?>
                </div>
              </div>
            </div>
          </div>
        </section>
    </article>
</section>
<?php endwhile; endif; ?>
<?php get_footer(); ?>

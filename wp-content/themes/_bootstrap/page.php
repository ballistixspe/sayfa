<?php
/**
* Page Template
*
* @package dash
*/ ?>
<?php get_header(); ?>
<section id="content" role="main">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        <?php get_template_part( 'content', 'page-header' ); ?>

        <section class="page-content">
            <?php the_content(); ?>
        </section>
    </article>
    <?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>

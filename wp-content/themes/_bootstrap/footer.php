<?php
/**
* Footer Template
*
* @package dash
*/ ?>
</div> <!-- /.container -->
</main> <!-- /#page-content -->
<footer id="site-footer" role="contentinfo">
<?php if (is_active_sidebar('widget-footer')): ?>
<aside id="footer-widget" role="complementary">
<div class="container">
<?php dynamic_sidebar('widget-footer'); ?>
</div>
</aside>
<?php endif; ?>
</footer>
</div> <!-- /#page-wrap -->
<?php wp_footer(); ?>
</body></html>

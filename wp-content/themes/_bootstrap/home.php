<?php
/**
* Page Template
*
* @package dash
*/ ?>
<?php get_header();?>

<?php get_template_part( 'content', 'page-header' ); ?>
<section id="news-featured" role="main">
    <!-- <h3 class="feature-title">Featured news</h3> -->
    <div class="row nav">
        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 4,
            'tag' => 'featured'
        );
        $the_query = new WP_Query( $args ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="entry">
                    <?php
                    $large_image_url = '';
                    if ( has_post_thumbnail() ) {
                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
                    }
                    ?>
                    <a href="<?php the_permalink(); ?>" class="content" style="background-image: url('<?php echo $large_image_url[0]; ?>')">
                        <div class="description">
                        <?php
                            $categories = get_the_category();
                            $cat_name = $categories[0]->cat_name; ?>
                            <span class="label label-warning"><?php echo $cat_name; ?></span>
                            <h3 class="title"><?php the_title(); ?></h3>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
    </div>
</section>

<div class="row">
    <section id="news-recent" role="main" class="col-sm-8">
       <div class="header clearfix">
            <h4 class="title">Recent news</h4>
            <ul class="post-categories list-inline">
            <?php
                $args = array(
                'show_option_all'    => '',
                'orderby'            => 'name',
                'order'              => 'ASC',
                'style'              => 'list',
                'show_count'         => 0,
                'hide_empty'         => 1,
                'use_desc_for_title' => 1,
                'child_of'           => 0,
                'feed'               => '',
                'feed_type'          => '',
                'feed_image'         => '',
                'exclude'            => '',
                'exclude_tree'       => '',
                'include'            => '',
                'hierarchical'       => 1,
                'title_li'           => '',
                'show_option_none'   => __( '' ),
                'number'             => null,
                'echo'               => 1,
                'depth'              => 0,
                'current_category'   => 0,
                'pad_counts'         => 0,
                'taxonomy'           => 'category',
                'walker'             => null
                );
                wp_list_categories( $args );
            ?>
            </ul>
       </div>
        <div class="row nav">
            <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3
            );
            $the_query = new WP_Query( $args ); ?>
            <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col-sm-4">
                    <a href="<?php the_permalink();?> " class="entry">
                        <?php
                        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                            the_post_thumbnail();
                        }
                        ?>
                            <h4><?php the_title(); ?></h4>
                    </a>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>
    </section>
    <section class="col-sm-4">
        <div class="well">
            <?php _dash_search_form(''); ?>
        </div>
    </section>
</div>


<?php get_footer(); ?>

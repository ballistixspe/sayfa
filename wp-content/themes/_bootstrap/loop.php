<?php
/**
* Post Loop Template
*
* @package dash
*/ ?>

<ul class="list-recent-post nav">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a href="<?php the_permalink() ?>"><?php the_title( ); ?></a>
</li>
<?php endwhile; endif; ?>
</ul>

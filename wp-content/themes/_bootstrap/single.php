<?php
/**
* Single Template
*
* @package dash
*/ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'content', 'page-header' ); ?>
<section id="content" role="main" class="col-sm-8">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


			<h2><?php the_title( ); ?></h2>
			<section class="entry-meta">
				<span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
			</section>

		<hr>

        <section class="page-content">
            <?php the_content(); ?>
        </section>
    </article>
</section>
<?php endwhile; endif; ?>
<aside class="col-sm-4">

	<h3>Latest Articles</h3>
	<hr>
	<?php
	$args = array(
	    'post_type' => 'post',
	    'posts_per_page' => 4
	);
	$the_query = new WP_Query( $args ); ?>
	<?php if ( $the_query->have_posts() ) : ?>
		<ul class="list-unstyled">
	    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	        <li>
	        <a href="<?php the_permalink();?> " class="entry">
	            <?php
	            if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
	                the_post_thumbnail();
	            }
	            ?>
	                <h4><?php the_title(); ?></h4>
	        </a>
	        </li>
	    <?php endwhile; ?>
	    </ul>
	    <?php wp_reset_postdata(); ?>
	<?php else : ?>
	    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>

</aside>
<?php get_footer(); ?>

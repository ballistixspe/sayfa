<?php
if( is_home() && get_option('page_for_posts') ) {
	$blog_page_id = get_option('page_for_posts');
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $blog_page_id ), 'full' );
	$featuredImage = $large_image_url[0];
	if ( !empty($large_image_url)  ) {
	    $featuredImage = $large_image_url[0];
	} else {
		$image = get_field('global_page_header_banner', 'option');
		$featuredImage = $image['url'];
	}

} else {

	global $wp_query;

	//echo $wp_query->query_vars['post_type'];
	if ($wp_query->query_vars['post_type'] == 'ufaq') {
		$featuredImage = "https://sayfa.com.au/wp-content/uploads/2019/02/forum5banner.jpg";
	}


	if ( has_post_thumbnail() ) {
	    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
	    $featuredImage = $large_image_url[0];
	} else {
		//$image = get_field('global_page_header_banner', 'option');
		//$featuredImage = $image['url'];
	}
} ?>
<header class="page-header full-row" data-stretch-type="full-stretched"
style="background-image: url( <?php echo $featuredImage; ?> );">


    <div class="page-title">

    <div class="container">


    	<?php
    if( is_home() && get_option('page_for_posts') ) {
    	$blog_page_id = get_option('page_for_posts');
    	echo '<h1 class="the_title">'.get_page($blog_page_id)->post_title.'</h1>';
    // } else if ( is_single() ){
    // 	echo '<h1 class="the_title">News</h1>';
    } else {
    	the_title('<h1 class="the_title">', '</h1>');
    }
    ?>


    </div>
    </div>
</header>
<section class="secondary-line full-row" data-stretch-type="full">

    <div class="row">

            <div class="col-sm-9">
                <?php _dash_breadcrumbs(); ?>
            </div>
            <div class="col-sm-3">
                <?php
                //echo '<form role="search" method="get" id="searchform" class="searchform" action="' . esc_url( home_url( '/' ) ) . '" ><input type="search" class="form-control search-field input-sm" placeholder="' . esc_attr_x( 'Search for Products', '_dash' ) . '" value="' . get_search_query() .'" name="s" title="'. esc_attr_x( 'Search for:', '_dash' ) .'" /></form>';
                 ?>
            </div>

    </div>

</section>

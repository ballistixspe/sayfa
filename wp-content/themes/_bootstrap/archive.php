<?php
/**
* Archive Template
*
* @package dash
*/ ?>

<?php get_header(); ?>

<section id="content-main" role="main">

    <header class="post-header">
        
        <h1 class="post-title">
        
        <?php
            if ( is_day() ) { 
                printf( __( 'Daily Archives: %s', '_dash' ), get_the_time( get_option( 'date_format' ) ) ); 
            } elseif ( is_month() ) { 
                printf( __( 'Monthly Archives: %s', '_dash' ), get_the_time( 'F Y' ) ); 
            } elseif ( is_year() ) { 
                printf( __( 'Yearly Archives: %s', '_dash' ), get_the_time( 'Y' ) ); 
            } else { 
                global $wp_query;
                _e($wp_query->queried_object->name);
                //_e( 'Archives', '_dash' ); 
            }
        ?>
        </h1>

    </header>

    <hr>
    
    <div class="row">
        
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
        <article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 col-sm-3'); ?> style="margin-bottom: 30px;">
            
            <?php if ( has_post_thumbnail() ) { the_post_thumbnail('medium'); } ?>

            <header class="post-header">

            <?php
            
                echo '<h4 class="post-title"><a href="' . get_the_permalink() .'" title="' . the_title_attribute(array( 'before' => 'Permalink to: ', 'after' => '', 'echo' => false )) .'" rel="bookmark">';
                the_title();
                echo '</a></h4>';
            ?>
            <section class="post-meta small">

                <span class="post-date">

                    <?php the_time( get_option( 'date_format' ) ); ?>
                    
                </span>
                
            </section>
            
            <hr>

            </header>

            <section class="post-summary">
    
                <?php the_excerpt(); ?>
                
                <?php
            
                echo '<a href="' . get_the_permalink() .'" title="' . the_title_attribute(array( 'before' => 'Permalink to: ', 'after' => '', 'echo' => false )) .'" rel="bookmark">Read More</a>';
                ?>

            </section>


        </article>
    
    <?php endwhile; endif; ?>

    </div>
    
    
    <div class="clearfix">&nbsp;</div>
    
    <?php _dash_pagination(); ?>

</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>

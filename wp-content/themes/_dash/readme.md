#_dash Blank Theme Framework for Wordpress

https://github.com/marcelbadua/_dash

Dash is a blank theme framework for Wordpress. This theme should be use with a child theme.

##Feature
- Site Information
- Built-in pagination and breadcrumb functions compatible with bootstrap class
- Widgets for sidebar and footer

##Updates

###2.0.0

------

If you like and using this theme, feel free to send me a hi, I would really appreciate that.
- [Twitter](https://twitter.com/marcelbadua)
- [marcelbadua](http://marcelbadua.com/)

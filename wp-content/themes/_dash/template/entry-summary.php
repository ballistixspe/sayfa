<section class="post-summary">
	
	<?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail'); } ?>

	<?php the_excerpt(); ?>

</section>

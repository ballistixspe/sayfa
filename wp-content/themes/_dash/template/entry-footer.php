<footer class="post-footer">

	<span class="cat-links"><?php _e( 'Categories: ', '_dash' ); ?><?php the_category( ', ' ); ?></span>

	<span class="tag-links"><?php the_tags(); ?></span>

	<div><?php edit_post_link(); ?></div>

</footer>

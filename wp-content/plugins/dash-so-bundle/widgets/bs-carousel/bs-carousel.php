<?php

/*
Widget Name: Bootstrap Carousel
Description: A bootstrap carousel
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class Bootstrap_Carousel_Widget extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'bootstrap-carousel-widget',
	        __('Bootstrap Carousel Widget', 'bootstrap-carousel-widget-text-domain'),
	        array(
	            'description' => __('A bootstrap carousel widget.', 'bootstrap-carousel-widget-text-domain'),
	            'help'        => 'http://example.com/bootstrap-carousel-widget-docs',
	        ),
	        array(),
			array(
                'slides' => array(
                    'type' => 'repeater',
                    'label' => __( 'Slides.' , 'widget-form-fields-text-domain' ),
                    'item_name'  => __( 'Slide', 'siteorigin-widgets' ),
                    'fields' => array(
                    	// 'title' => array(
                    	//     'type' => 'text',
                    	//     'label' => __('Title.', 'widget-form-fields-text-domain')
                    	// ) ,
                        

                        'type' => array(
                            'type' => 'select',
                            'label' => __( 'Background Type', 'siteorigin-widgets' ),
                            'options' => array(
                                'image' => __( 'Image', 'siteorigin-widgets' ),
                                'video' => __( 'Video', 'siteorigin-widgets' ),
                            ),
                            'state_emitter' => array(
                                'callback' => 'select',
                                'args' => array( 'field_type_{$repeater}' ),
                            )
                        ),

						'image' => array(
						    'type' => 'media',
						    'label' => __( 'Choose a background image.', 'widget-form-fields-text-domain' ),
						    'choose' => __( 'Choose image', 'widget-form-fields-text-domain' ),
						    'update' => __( 'Set image', 'widget-form-fields-text-domain' ),
						    'library' => 'image',
						    'fallback' => true,

                            'state_handler' => array(
                                'field_type_{$repeater}[image]' => array( 'show' ),
                                '_else[field_type_{$repeater}]' => array( 'hide' ),
                            ),
						),

                        'caption' => array(
                            'type' => 'tinymce',
                            'label' => __( 'Caption', 'widget-form-fields-text-domain' ),
                            'default' => 'An example of a long message.</br>It is even possible to add a few html tags.</br><a href="siteorigin.com" target="_blank"">Links!</a>',
                            'rows' => 10,
                            'default_editor' => 'html',
                            'button_filters' => array(
                                'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                                'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                                'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                                'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                                'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                            ),
                            'state_handler' => array(
                                'field_type_{$repeater}[image]' => array( 'show' ),
                                '_else[field_type_{$repeater}]' => array( 'hide' ),
                            ),
                        ),

                        'video' => array(
                            'type' => 'text',
                            'label' => __('Youtube Video ID', 'widget-form-fields-text-domain'),
                            'state_handler' => array(
                                'field_type_{$repeater}[video]' => array( 'show' ),
                                '_else[field_type_{$repeater}]' => array( 'hide' ),
                            ),
                        )
                    ),

                ),
	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return '';
    }
}

siteorigin_widget_register('bootstrap-carousel-widget', __FILE__, 'Bootstrap_Carousel_Widget');
<?php
if ( ! empty( $instance['slides'] ) ) :
$slides = $instance['slides'];
$uniqid =  uniqid();
?>
<div id="carousel-<?php echo $uniqid; ?>" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
<?php for ($i=0; $i < count($slides); $i++) { ?>
<li data-target="#carousel-<?php echo $uniqid; ?>" data-slide-to="<?php echo $i; ?>" class=""></li>
<?php } ?>
</ol>
<div class="carousel-inner">
<?php foreach ( $slides as $index => $slide ) :  ?>
<?php if ($slide['type'] == 'video') { ?>
<div class="item video-background" data-video="<?php echo $slide['video'] ?>">
<?php  } else if ($slide['type'] == 'image') { ?>
<?php $image =  wp_get_attachment_image_src( $slide['image'] , 'full' ); ?>
<div class="item image-background" style="background-image: url(<?php echo $image[0]; ?>)">
<?php } ?>

<div class="carousel-caption">
<?php if ( ! empty($slide['title']) ): ?>
<h1><?php echo $slide['title']; ?></h1>
<?php endif; ?>
<?php echo wpautop($slide['caption']); ?>
<?php if ( ! empty($slide['button_link']) && ! empty($slide['button_text']) ): ?>
<a href="<?php echo $slide['button_link'] ?>" class="btn btn-default">
<?php echo $slide['button_text'] ?>
</a>
<?php endif; ?>
</div> <!-- /carousel-caption -->
</div> <!-- /item -->
<?php endforeach; ?>
</div> <!-- /carousel-inner -->
<a class="left carousel-control" href="#carousel-<?php echo $uniqid; ?>" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a class="right carousel-control" href="#carousel-<?php echo $uniqid; ?>" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div> <!-- /carousel -->
<script>
jQuery('.carousel .carousel-inner .item:first-child').addClass('active');
jQuery('.carousel .carousel-indicators li:first-child').addClass('active');
</script>
<?php endif; ?>

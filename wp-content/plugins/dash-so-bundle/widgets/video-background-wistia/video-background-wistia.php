<?php

/*
Widget Name: Video Background: Wistia
Description: Video Background that uses videos from Wistia
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class Video_Background_Wistia_Widget extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'video-background-wistia-widget',
	        __('Video Background: Wistia Widget', 'video-background-wistia-widget-text-domain'),
	        array(
	            'description' => __('Video Background that uses videos from Wistia.', 'video-background-wistia-widget-text-domain'),
	            //'help'        => 'http://example.com/video-background-wistia-widget-docs',
	        ),
	        array(),
			array(
				'overlayVideo' => array(
	                'type' => 'text',
	                'label' => __('Overlay Video ID', 'video-background-wistia-widget-text-domain')
	            ),
    			'backgroundvideo' => array(
                    'type' => 'text',
                    'label' => __('Background Video ID', 'video-background-wistia-widget-text-domain')
                )
	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

	function enqueue_frontend_scripts( $instance ) {

	    wp_enqueue_script(
	        'video-background-wistia-widget',
	        plugin_dir_url(__FILE__).'js/script.js',
	        array( 'jquery' ),
	        '1.0.0',
	        TRUE
	    );

	    parent::enqueue_frontend_scripts( $instance );
	}
    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return 'style';
    }
}

siteorigin_widget_register('video-background-wistia-widget', __FILE__, 'Video_Background_Wistia_Widget');

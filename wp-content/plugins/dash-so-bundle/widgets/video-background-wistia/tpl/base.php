<div id="video_container" data-overlayvideo="<?php echo $instance['overlayVideo']; ?>" data-backgroundvideo="<?php echo $instance['backgroundvideo']; ?>">
  <div id="text">
    <div id="actions">
      <div id="playbutton">
        <div class="rectangle"></div>
        <div class="triangle"></div>
      </div>
    </div>
  </div>
  <div id="cover_all"></div>
  <div id="main-image"></div>
  <div id="wistia_<?php echo $instance['backgroundvideo']; ?>" class="wistia_embed backgroundVideo" style="width:920px;height:518px;"></div>
  <div id="wistia_<?php echo $instance['overlayVideo']; ?>" class="wistia_embed overlayVideo" style="width:1920px;height:1080px;"></div>
  <div id="ex">&times;</div>
</div>
<script charset='ISO-8859-1' src='https://fast.wistia.com/assets/external/E-v1.js'></script>
<script charset='ISO-8859-1' src='https://fast.wistia.com/labs/crop-fill/plugin.js'></script>
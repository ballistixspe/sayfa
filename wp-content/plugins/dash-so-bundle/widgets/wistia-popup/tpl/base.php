<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
<?php
$class = '';
$style = '';
$class .= ' popoverContent=' . $instance['popoverContent'] . ' ' . $instance['popoverContent'];

if ($instance['popoverContent'] == 'thumbnail') {
	$style .= 'width:' . $instance['width'] . '; height:' . $instance['height'] . ';';
	$content = '&nbsp;';
} else {
	$style .= 'display:block; white-space:wrap;';
	$content .= '<figure style="cursor: pointer;">';
	$content .= wp_get_attachment_image( $instance['thumbnail'] , 'large' );
	$content .= '<figcaption>' . wpautop ($instance['caption']) . '</figcaption>';
	$content .= '</figure>';
}

printf('<div class="wistia_embed wistia_async_%1$s popover=true %2$s" style="%3$s">%4$s</div> ', $instance['video_id'],$class,$style,$content);
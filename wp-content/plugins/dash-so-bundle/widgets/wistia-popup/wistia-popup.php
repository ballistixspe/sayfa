<?php

/*
Widget Name: Wistia Popup widget
Description: Wistia Popup widget
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class WISTIA_POPUP_WIDGET extends SiteOrigin_Widget
{

    function __construct()
    {
        parent::__construct(
          'wistia-popup-widget',
          __('Wistia Popup Widget', 'text-domain'),
          array(
            'description' => __('Wistia Popup widget.', 'text-domain')
          ),
          array(),
          array(
            'video_id' => array(
                'type' => 'text',
                'label' => __('Video ID', 'text-domain')
            ),
            
            'popoverContent' => array(
                'type' => 'select',
                'label' => __( 'popoverContent', 'widget-form-fields-text-domain' ),
                'default' => 'thumbnail',
                'state_emitter' => array(
                    'callback' => 'select',
                    'args' => array( 'popoverContent' )
                ),
                'options' => array(
                    'html' => __( 'html', 'widget-form-fields-text-domain' ),
                    'thumbnail' => __( 'thumbnail', 'widget-form-fields-text-domain' ),
                )
            ),
            'width' => array(
                'type' => 'text',
                'label' => __('Width', 'text-domain'),
                'default' => '100%',
                'state_handler' => array(
                    'popoverContent[html]' => array('hide'),
                    'popoverContent[thumbnail]' => array('show'),
                ),
            ),
            'height' => array(
                'type' => 'text',
                'label' => __('Height', 'text-domain'),
                'default' => '280px',
                'state_handler' => array(
                    'popoverContent[html]' => array('hide'),
                    'popoverContent[thumbnail]' => array('show'),
                ),
            ),
            'caption' => array(
                'type' => 'textarea',
                'label' => __( 'Type a message', 'widget-form-fields-text-domain' ),
                'default' => '',
                'rows' => 10,
                'state_handler' => array(
                    'popoverContent[html]' => array('show'),
                    'popoverContent[thumbnail]' => array('hide'),
                ),
            ),
            'thumbnail' => array(
                'type' => 'media',
                'label' => __( 'Choose thumbnail', 'widget-form-fields-text-domain' ),
                'choose' => __( 'Choose thumbnail', 'widget-form-fields-text-domain' ),
                'update' => __( 'Set thumbnail', 'widget-form-fields-text-domain' ),
                'library' => 'image',
                'fallback' => true,
                'state_handler' => array(
                    'popoverContent[html]' => array('show'),
                    'popoverContent[thumbnail]' => array('hide'),
                ),
            )
          ),
          plugin_dir_path(__FILE__)
        );
    }

    function initialize() {
        
        $this->register_frontend_styles(
            array(
                array(
                    'wistia-widget',
                    plugin_dir_url(__FILE__) . 'styles/styles.css',
                    array(),
                    '0.0.1'
                ),
            )
        );
    }
    
    function get_template_name($instance)
    {
        return 'base';
    }

    function get_style_name($instance)
    {
        return '';
    }
}

siteorigin_widget_register('wistia-popup-widget', __FILE__, 'WISTIA_POPUP_WIDGET');

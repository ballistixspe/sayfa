<?php

/*
Widget Name: Wistia widget
Description: Wistia widget
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class WISTIA_WIDGET extends SiteOrigin_Widget
{

    function __construct()
    {
        parent::__construct(
          'wistia-widget',
          __('Wistia Widget', 'text-domain'),
          array(
            'description' => __('Wistia widget.', 'text-domain')
          ),
          array(),
          array(
            'video_id' => array(
                'type' => 'text',
                'label' => __('Video ID', 'text-domain')
            ),
            'popoverContent' => array(
                'type' => 'select',
                'label' => __( 'popoverContent', 'widget-form-fields-text-domain' ),
                'default' => 'thumbnail',
                'state_emitter' => array(
                    'callback' => 'select',
                    'args' => array( 'popoverContent' )
                ),
                'options' => array(
                    'html' => __( 'html', 'widget-form-fields-text-domain' ),
                    'thumbnail' => __( 'thumbnail', 'widget-form-fields-text-domain' ),
                )
            ),
            'width' => array(
                'type' => 'text',
                'label' => __('Width', 'text-domain'),
                'default' => '100%',
                'state_handler' => array(
                    'popoverContent[html]' => array('hide'),
                    'popoverContent[thumbnail]' => array('show'),
                ),
            ),
            'height' => array(
                'type' => 'text',
                'label' => __('Height', 'text-domain'),
                'default' => '280px',
                'state_handler' => array(
                    'popoverContent[html]' => array('hide'),
                    'popoverContent[thumbnail]' => array('show'),
                ),
            ),
            'caption' => array(
                'type' => 'textarea',
                'label' => __( 'Type a message', 'widget-form-fields-text-domain' ),
                'default' => '',
                'rows' => 10,
                'state_handler' => array(
                    'popoverContent[html]' => array('show'),
                    'popoverContent[thumbnail]' => array('hide'),
                ),
            ),
            'thumbnail' => array(
                'type' => 'media',
                'label' => __( 'Choose thumbnail', 'widget-form-fields-text-domain' ),
                'choose' => __( 'Choose thumbnail', 'widget-form-fields-text-domain' ),
                'update' => __( 'Set thumbnail', 'widget-form-fields-text-domain' ),
                'library' => 'image',
                'fallback' => true,
                'state_handler' => array(
                    'popoverContent[html]' => array('show'),
                    'popoverContent[thumbnail]' => array('hide'),
                ),
            )
          ),
          plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance)
    {
        return 'base';
    }

    function get_style_name($instance)
    {
        return '';
    }
}

siteorigin_widget_register('wistia-widget', __FILE__, 'WISTIA_WIDGET');

<?php

/*
Widget Name: Button
Description: A button widget
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class Button_Widget extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'button-widget',
	        __('Button Widget', 'button-widget-text-domain'),
	        array(
	            'description' => __('A button widget.', 'button-widget-text-domain'),
	            'help'        => 'http://example.com/button-widget-docs',
	        ),
	        array(),
			array(

            	'text' => array(
            	    'type' => 'text',
            	    'label' => __('Text.', 'widget-form-fields-text-domain')
            	),

                'link' => array(
                    'type' => 'text',
                    'label' => __('Link.', 'widget-form-fields-text-domain')
                ),
                'class' => array(
                    'type' => 'select',
                    'label' => __( 'Button Type', 'widget-form-fields-text-domain' ),
                    'default' => 'btn-default',
                    'options' => array(
                        'btn-default' => __( 'Default', 'widget-form-fields-text-domain' ),
                        'btn-primary' => __( 'Primary', 'widget-form-fields-text-domain' ),
                        'btn-warning' => __( 'Warning', 'widget-form-fields-text-domain' ),
                        'btn-info'    => __( 'Info', 'widget-form-fields-text-domain' ),
                        'btn-success' => __( 'Success', 'widget-form-fields-text-domain' ),
                    )
                )

	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return '';
    }
}

siteorigin_widget_register('button-widget', __FILE__, 'Button_Widget');
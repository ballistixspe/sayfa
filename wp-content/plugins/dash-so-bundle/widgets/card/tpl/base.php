<?php
if ($instance['new_window']) $additional_attribute = ' target="_blank"';

if($instance['block_link']) echo '<a href=' . sow_esc_url($instance['url'] ) . $additional_attribute . '>';
if($instance['image']) printf('<div class="card-image">%1$s</div>',wp_get_attachment_image( $instance['image'] , $instance['image_size'] ));

echo '<div class="card-content">';

if($instance['title']) printf('<div class="title"><h3>%1$s</h3></div>', $instance['title']);

echo '<div class="content">';

$content = wpautop($instance['text'] );
echo $content;

if( !($instance['block_link'])) echo '<a class="link-button" href=' . sow_esc_url($instance['url'])  . $additional_attribute . '>' . $instance['link_button_text'] . '</a>';

echo '</div></div>';

if($instance['block_link']) echo '</a>'?>

<?php

/*
Widget Name: List
Description: A list widget
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class List_Widget extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'list-widget',
	        __('List Widget', 'list-widget-text-domain'),
	        array(
	            'description' => __('A list widget.', 'list-widget-text-domain'),
	            'help'        => 'http://example.com/list-widget-docs',
	        ),
	        array(),
			array(

            	'title' => array(
            	    'type' => 'text',
            	    'label' => __('Title.', 'widget-form-fields-text-domain')
            	) ,
                'text' => array(
                    'type' => 'tinymce',
                    'label' => __( 'text', 'widget-form-fields-text-domain' ),
                    'default' => 'An example of a long message.</br>It is even possible to add a few html tags.</br><a href="siteorigin.com" target="_blank"">Links!</a>',
                    'rows' => 10,
                    'default_editor' => 'html',
                    'button_filters' => array(
                        'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                        'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                        'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                        'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                        'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                    ),
                ),
				'image' => array(
				    'type' => 'media',
				    'label' => __( 'Choose an image.', 'widget-form-fields-text-domain' ),
				    'choose' => __( 'Choose image', 'widget-form-fields-text-domain' ),
				    'update' => __( 'Set image', 'widget-form-fields-text-domain' ),
				    'library' => 'image',
				    'fallback' => true
				),


	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return '';
    }
}

siteorigin_widget_register('list-widget', __FILE__, 'List_Widget');
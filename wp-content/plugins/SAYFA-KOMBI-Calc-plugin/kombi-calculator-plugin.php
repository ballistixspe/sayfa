<?php
/*
Plugin Name: SAYFA KOMBI Calculator Plugin
Plugin URI: https://www.sayfa.com.au
Description: Use the KOMBI Calculator to calculate that the rise, going, tread markings, etc. are within the Australian Standards for Stair configurations.
Version: 1.2
Author: Steven Branigan
Author URI: http://www.stevenbranigan.com/
*/


add_action('admin_menu', 'kombi_admin_settings');

function kombi_admin_settings() {
    $page_title = 'KOMBI Calculator Admin Settings';
    $menu_title = 'KOMBI Calculator Admin Settings';
    $capability = 'edit_posts';
    $menu_slug = 'kombi_calculator_admin';
    $function = 'kombi_calculator_admin_page_display';
    $icon_url = '';
    $position = 24;

    add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
}

function kombi_calculator_admin_page_display() {

        // Checks the kombi-form settings
    if (isset($_POST['kombi_form'])) {
        update_option('kombi_form', $_POST['kombi_form']);
        $kombi_form = $_POST['kombi_form'];
    } 

    $kombi_form = get_option('kombi_form', 'not set');

    // checks the kombi-options page settings
    if (isset($_POST['kombi_options'])) {
        update_option('kombi_options', $_POST['kombi_options']);
        $kombi_options = $_POST['kombi_options'];
    } 

    $kombi_options = get_option('kombi_options', 'not set');

    // checks the kombi-roof-results options
    if (isset($_POST['kombi_roof_results'])) {
        update_option('kombi_roof_results', $_POST['kombi_roof_results']);
        $kombi_roof_results = $_POST['kombi_roof_results'];
    } 

    $kombi_roof_results = get_option('kombi_roof_results', 'not set');

    // checks the kombi-height-results options
    if (isset($_POST['kombi_height_results'])) {
        update_option('kombi_height_results', $_POST['kombi_height_results']);
        $kombi_height_results = $_POST['kombi_height_results'];
    } 

    $kombi_height_results = get_option('kombi_height_results', 'not set');


    include 'kombi-settings-form.php';
}

class PageTemplater {


        /**
         * A reference to an instance of this class.
         */
        private static $instance;

        /**
         * The array of templates that this plugin tracks.
         */
        protected $templates;

        /**
         * Returns an instance of this class. 
         */
        public static function get_instance() {

                if ( null == self::$instance ) {
                        self::$instance = new PageTemplater();
                } 

                return self::$instance;

        } 

        /**
         * Initializes the plugin by setting filters and administration functions.
         */
        private function __construct() {

                $this->templates = array();


                // Add a filter to the attributes metabox to inject template into the cache.
                if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

                        // 4.6 and older
                        add_filter(
                                'page_attributes_dropdown_pages_args',
                                array( $this, 'register_project_templates' )
                        );

                } else {

                        // Add a filter to the wp 4.7 version attributes metabox
                        add_filter(
                                'theme_page_templates', array( $this, 'add_new_template' )
                        );

                }

                // Add a filter to the save post to inject out template into the page cache
                add_filter(
                        'wp_insert_post_data', 
                        array( $this, 'register_project_templates' ) 
                );


                // Add a filter to the template include to determine if the page has our 
                // template assigned and return it's path
                add_filter(
                        'template_include', 
                        array( $this, 'view_project_template') 
                );


                // Add your templates to this array.
                $this->templates = array(
                        //'goodtobebad-template.php' => 'Its Good to Be Bad',
                        'templates/KOMBI-Calc-Form.php' => 'kombi-form-plugin',
                        'templates/KOMBI-Calculator.php' => 'kombi-options-plugin',
                        'templates/KOMBI-Calc-Height-Only-Result.php' => 'kombi-results-height-plugin',
                        'templates/KOMBI-Calc-Roof-Stair-Result.php' => 'kombi-results-roof-stair',
                );
                        
        } 

        /**
         * Adds our template to the page dropdown for v4.7+
         *
         */
        public function add_new_template( $posts_templates ) {
                $posts_templates = array_merge( $posts_templates, $this->templates );
                return $posts_templates;
        }

        /**
         * Adds our template to the pages cache in order to trick WordPress
         * into thinking the template file exists where it doens't really exist.
         */
        public function register_project_templates( $atts ) {

                // Create the key used for the themes cache
                $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

                // Retrieve the cache list. 
                // If it doesn't exist, or it's empty prepare an array
                $templates = wp_get_theme()->get_page_templates();
                if ( empty( $templates ) ) {
                        $templates = array();
                } 

                // New cache, therefore remove the old one
                wp_cache_delete( $cache_key , 'themes');

                // Now add our template to the list of templates by merging our templates
                // with the existing templates array from the cache.
                $templates = array_merge( $templates, $this->templates );

                // Add the modified cache to allow WordPress to pick it up for listing
                // available templates
                wp_cache_add( $cache_key, $templates, 'themes', 1800 );

                return $atts;

        } 

        /**
         * Checks if the template is assigned to the page
         */
        public function view_project_template( $template ) {
                
                // Get global post
                global $post;

                // Return template if post is empty
                if ( ! $post ) {
                        return $template;
                }

                // Return default template if we don't have a custom one defined
                if ( ! isset( $this->templates[get_post_meta( 
                        $post->ID, '_wp_page_template', true 
                )] ) ) {
                        return $template;
                } 

                $file = plugin_dir_path( __FILE__ ). get_post_meta( 
                        $post->ID, '_wp_page_template', true
                );

                // Just to be safe, we check if the file exist first
                if ( file_exists( $file ) ) {
                        return $file;
                } else {
                        echo $file;
                }

                // Return template
                return $template;

        }

} 
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );

?>
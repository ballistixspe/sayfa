﻿<?php
/**
* Template Name: KOMBI Calc Form Plugin
* @package dash
*/ ?>

<?php get_header(); ?>

<section id="content-main" role="main">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part( 'template/entry' ); ?>
    
    <?php endwhile; endif; ?>

        <script type="text/javascript"> 
            function displayForm(c) {
                if (c.value == "1") {
                    document.getElementById("KOMBI_Height_Angle").style.visibility = 'visible';
                    document.getElementById("KOMBI_Height_Only").style.visibility = 'hidden';
                    document.getElementById("KOMBI_Height_Base").style.visibility = 'hidden';
                } else if (c.value == "2") {
                    document.getElementById("KOMBI_Height_Angle").style.visibility = 'hidden';
                    document.getElementById("KOMBI_Height_Base").style.visibility = 'hidden';
                    document.getElementById("KOMBI_Height_Only").style.visibility = 'visible';
                } else if (c.value == "3") {
                    document.getElementById("KOMBI_Height_Angle").style.visibility = 'hidden';
                    document.getElementById("KOMBI_Height_Base").style.visibility = 'visible';
                    document.getElementById("KOMBI_Height_Only").style.visibility = 'hidden';
                } else {}
            }
        </script>
<!-- <div style="width:800px;"> -->
        <div style="padding:30px;">
            <form>
                <input value="2" type="radio" name="formselector" onClick="displayForm(this)"></input>Height - Must be less than 4050mm
            </form>
        </div>

        <?php

        if (isset($_POST['kombi_options'])) {
                        update_option('kombi_options', $_POST['kombi_options']);
                        $kombi_options = $_POST['kombi_options'];
                    } 

                    $kombi_options = get_option('kombi_options', 'not set');

        echo 

        "
            <br>
            <br>
            
            <!-- This is the form for the fixed pitch angle and length calculator -->
            <div style='visibility:hidden; position:relative; top:-45px;margin-top:-45px; padding:30px; clear:both;' id='KOMBI_Height_Angle'>
                <form id='KOMBI_Height_Angle' method='post' action='" . $kombi_options . "'>
                    
                    

                        <img src='http://sayfa.com.au/wp-content/uploads/2017/11/KOMBI-L-A-300x240.png' style='text-align: left;'>

                    
                    
                    

                        <p>
                            <label style='display:inline-block; width:100%;''>Fixed Pitch (Angle) - Must be between 20° and 45°: </label>
                            <input type='number' name='fixedPitch' min='20' max='45' style='width:100%;'>
                            
                        </p>
                        <p>
                            <label style='display:inline-block; width:100%;''>Length (mm): </label>
                            <input type='number' name='hypotenuse' min='0' max='10000' style='width:100%;''>
                            
                        </p>
                        <p align='left'>
                            <input type='submit' value='SUBMIT'>
                        </p>

                    

                </form>
            </div>

            
            <br>
            <br>
            <!-- This is the form for the height only calculator -->

            <div style='visibility:hidden;position:relative;top:-280px;margin-top: -280px; padding:30px; clear:both;' id='KOMBI_Height_Only'>

                <form id='KOMBI_Height_Only' method='post' action= '" . $kombi_options . "'>
                    
                    

                        <img src='http://sayfa.com.au/wp-content/uploads/2017/11/KOMBI-H-FP-300x240.png'>
                        
                    
                    
                    

                        <p>
                            <label style='display:inline-block; width:100%;'>Height (mm) - Must be less than 4050mm: </label>
                            <input type='number' name='opposite' min='0' max='4050' style='width:100%;'>
                        </p>

                        <p align='left'>
                            <input type='submit' value='SUBMIT'>
                        </p>

                    
                </form>
            </div>

            <br>
            <br>
            <!-- This is the form for the height and footprint calculator -->

            <div style='visibility:hidden;position:relative;top:-380px;margin-top: -380px; padding:30px; clear:both;' id='KOMBI_Height_Base'>

                <form id='KOMBI_Height_Base' method='post' action='" . $kombi_options . "'>

                    

                        <img src='http://sayfa.com.au/wp-content/uploads/2017/11/KOMBI-H-FP-300x240.png'>

                    

                    

                        <p>
                            <label style='display:inline-block; width:100%;'>Height (mm): </label>
                            <input type='number' name='opposite' min='0' max='4050' style='width:100%;'>
                           
                        </p>
                        <p>
                            <label style='display:inline-block; width:100%'>Footprint (mm): </label>
                            <input type='number' name='adjacent' min='0' style='width:100%;'>
                            
                        </p>
                        <p align='left'>
                            <input type='submit' value='SUBMIT'>
                        </p>

                    
                </form>

            </div>";

        ?>
<!-- </div> -->
</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>



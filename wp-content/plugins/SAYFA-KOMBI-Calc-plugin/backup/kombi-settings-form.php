<!-- // form-file.php -->
<h1>KOMBI Calculator Options Page</h1>
<br>
<h3>Using the settings page</h3>
<p>First, create a KOMBI Calculator page for the KOMBI Calculator Form<p>

    <?php
    echo '<img src="' . plugins_url( 'images/kombi-add-form.png', __FILE__ ) . '"  height="auto" width="300" border="1"> ';
    ?>
<p>Create a WordPress page for the KOMBI Options, using the KOMBI-Options-Plugin template<p>
    <?php
    echo '<img src="' . plugins_url( 'images/kombi-add-options.png', __FILE__ ) . '" height="auto" width="1200" border="1"> ';
    ?>
<p>Use the WordPress page URL and copy that into the "Calculator Options Page" textbox.  This is used to redirect the form to the options page.<p>
    <?php
    echo '<img src="' . plugins_url( 'images/kombi-add-options-url.png', __FILE__ ) . '" height="auto" width="300" border="1"> ';
    ?>
<br>
<br>
<form method="POST">
    <p>The calculator options page is the first page the user is directed to when the calculator form is submitted.  Here they are presented
        with options for angle of stair and number of rises applicable.</p>
    <strong><label for="kombi_options" width="500">Calculator Options Page:</label></strong>
    <input type="text" name="kombi_options" id="kombi_options" value="<?php echo $kombi_options; ?>">
    <br>
    <p>KOMBI Roof Stair Results are for KOMBI Stair installations where the stair is fixed to a roof.  It is not a standard free standing stair installation.
        Front and back markings are calcuated differently to the standard roof stair installation.</p>
    <strong><label for="kombi_roof_results">Roof Stair Results:</label></strong>
    <input type="text" name="kombi_roof_results" id="kombi_roof_results" value="<?php echo $kombi_roof_results; ?>">
    <br>
    <p>Standard KOMBI Stair Results are for standard free standing KOMBI installations.  These results calculate the tread markings differently to the KOMBI Stair Results page</p>
    <strong><label for="kombi_height_results" width="500">Standard Stair Results:</label></strong>
    <input type="text" name="kombi_height_results" width="500" id="kombi_height_results" value="<?php echo $kombi_height_results; ?>">
    <br>
    <br>
    <br>
    <input type="submit" value="Save" class="button button-primary button-large">
</form>

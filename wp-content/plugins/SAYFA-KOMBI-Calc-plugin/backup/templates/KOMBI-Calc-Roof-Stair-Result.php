<?php
/**
* Template Name: KOMBI Calc Roof Stair Result Plugin
 *
* @package dash
*/ ?>

<?php get_header(); ?>

<section id="content-main" role="main">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part( 'template/entry' ); ?>
    
    <?php endwhile; endif; ?>

    <!-- 

        This page presents the calculations for SAYFA KOMBI Stairs based on known angle, height and selected number of rises / treads.

    -->

    <div>

        <!-- These are the KOMBI Stair Dimensions -->
        <div>

            <?php

            include 'KOMBI-Calc-Functions.php';

            session_start();

            // Gets the following variables from url
            $treadDepth = 40;
            $hypotenuse = $_GET['hypotenuse'];
            $fixedPitch = (int) $_GET['fixedPitch'];
            $treads = $_GET['treads'];

            $treadDepth = 40;
                      

                    $opposite = calcOpposite_FromAngleHypotenuse($fixedPitch, $hypotenuse);
                    $adjacent = calcAdjacent_FromOppositeHypotenuse($opposite, $hypotenuse);

                    $sin = calcSine($opposite, $hypotenuse);
                    /* Calculates the lower bracket adjacent */
                    $lowerBracketAdjacent = calcLowerBracketAdjacent($sin);
                    /* Calculates the upper bracket dimensions */
                    // /$sine = rad2deg($sin);
                    $upperBracketAdjacent = calcUpperBracketAdjacent($fixedPitch);
                    /* Calculates the cut stringer length */
                    $cutStringerLength = $hypotenuse;
                    /* calculates the tread spacing */
                    /* Calculates the read rise and run */
                    $rise = calcRiseWtreads($opposite, $treads);
                    $run = calcTreadSpacingAdjacent($rise, $sin);
                    /* calculates the tread spacing */
                    $treadSpacing = calcTreadSpacing($run, $rise, $sin);
                    $riseHypotenuse = calcTreadSpacingHypotenuse($rise, $run, $treadDepth);
                    /* Calculates the tread measurements */
                    $firstFrontMeasurement = calcFirstFrontMeasurement($rise, $treadDepth);
                    $firstFrontMeasurementAdjacent = calcFirstFrontMeasurementAdjacent($firstFrontMeasurement, $sin);
                    $firstFrontMeasurementHypotenuse = calcFirstFrontMeasurementHypotenuse_RoofStair($firstFrontMeasurement, $firstFrontMeasurementAdjacent);
                    
                    $totalMeasurement = ($firstFrontMeasurementHypotenuse - $lowerBracketAdjacent);

                        if ($totalMeasurementCount >= 0){
                            $totalMeasurement = $totalMeasurement;
                            $kombiImage1_option = 0;

                        }
                        else {

                        }
                        if ($totalMeasurement < 0){
                            $totalMeasurement = $totalMeasurement + 50;
                            $kombiImage1_option = 1;
                            $cutStringerLength = $cutStringerLength + 50;
                        } else {
                            
                        }

                        if ($totalMeasurement < 0){
                            $totalMeasurement = $totalMeasurement + 50;
                            $kombiImage1_option = 2;
                            $cutStringerLength = $cutStringerLength + 50;
                        } else {
                            
                        }

                    // gets the correct images for the display results page
                    $kombiImage1 = kombiImage1($kombiImage1_option);
                    $kombiImage3d = kombiImage3d();

                    // TO DO: Create validation funtion to validate the values
                    if (validateAngle($sin)){
                        $err = validatationErrMsg(0, $sin, $run, $treads, $rise);
                        echo "$err";
                        $redirect = getErrRedirectionURL();
                        echo "$redirect";
                    } else if (validateRun($run)){
                        $err = validatationErrMsg(1, $sin, $run, $treads, $rise);
                        echo "$err";
                        $redirect = getErrRedirectionURL();
                        echo "$redirect";
                    } else if (validateTreads($treads)){
                        $err = validatationErrMsg(2, $sin, $run, $treads, $rise);
                        echo "$err";
                        $redirect = getErrRedirectionURL();
                        echo "$redirect";
                    } else if (validateRise($rise)){
                        $err = validatationErrMsg(3, $sin, $run, $treads, $rise);
                        echo "$err";
                        $redirect = getErrRedirectionURL();
                        echo "$redirect";
                    } else if (validateRiseGoingCombination($rise, $run)){
                        $err = validatationErrMsg(4, $sin, $run, $treads, $rise);
                        echo "$err";
                        $redirect = getErrRedirectionURL();
                        echo "$redirect";
                    } else {
                    
                    echo "<div>";

                    echo "<div style='float:right;'>";
                        echo "<img src='" . $kombiImage1 . "' style='max-width:100%; height:auto;' alt='KOMBI Stair Markings Instructions'>";
                    echo "</div>";

                    echo "<div style='float:left; width:300px'>";
                        //echo "<br>";  echo "<br>";  echo "<br>";  echo "<br>";  echo "<br>";
                        echo "<h3>KOMBI STAIR DIMENSIONS</h3>";
                        // Prints table with main information related to the stair
                        echo "<table>";
                            echo "<tr><td><strong>(H) Height: </strong></td><td>" . round($opposite) . " mm</td></tr>";
                            echo "<tr><td><strong>(L) Length: </strong></td><td>" . round($adjacent) . " mm</td></tr>";
                            echo "<tr><td><strong>(S) Cut Stringer: </strong></td><td>" . round($cutStringerLength) . " mm</td></tr>";
                            echo "<tr><td><strong> Tread Count: </strong></td><td>" . round($treads) . "</td></tr>";
                            echo "<tr><td><strong>(A) Stairs Angle: </strong></td><td>" . round($fixedPitch) . "°</td></tr>";
                            echo "<tr><td><strong>(R) Rise: </strong></td><td>" . round($rise) . " mm</td></tr>";
                            echo "<tr><td><strong>(G) Going: </strong></td><td>" . round($run) . " mm</td></tr>";
                        echo "</table>";

                        echo "<img src='" . $kombiImage3d . "' style='max-width:100%; height:auto;' alt='KOMBI Stair 3D Profile'><br>";

                    echo "</div>";

                    echo "<div style='width:100%;'>";

                    // TO DO:  Change the $totalMeausrement based on whether the first marking is negative

                        


                        $totalMeasurementCount = 1;

                        echo "<table>";
                        echo "<tr><td colspan='3'><h3>TREAD MEASUREMENTS</h3></td></tr>";
                        echo "<tr><td><strong>TREAD</strong></td><td><strong>FRONT MARKING</strong></td><td><strong>BACK MARKING</strong></td></tr>";
                        echo "<tr><td>1</td><td style='color:red;'><strong>F1 = " . round($totalMeasurement) . " mm</strong></td><td style='color:blue;'><strong>B1 = " . round($firstFrontMeasurementHypotenuse) . " mm</strong></td></tr>";
                        $backMarking = ($firstFrontMeasurementHypotenuse);
                        WHILE ($totalMeasurementCount < $treads)
                            {
                                $backMarking = ($backMarking + $riseHypotenuse);
                                $totalMeasurement = ($totalMeasurement + $treadSpacing);
                                echo "<tr><td>" . ($totalMeasurementCount + 1) . "</td><td style='color:red;'><strong>F" . ($totalMeasurementCount + 1) . " = " . round($totalMeasurement) . " mm </strong></td><td style='color:blue;'><strong>B" . ($totalMeasurementCount + 1) . " = " . round($backMarking) . " mm</strong></td></tr>";
                                $totalMeasurementCount++;

                            }
                        echo "</table>";
                    echo "</div>";
                    }

            ?>

        </div>

    </div>

    <!-- to here -->

</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>


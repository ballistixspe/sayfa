<?php

function kombiImage1($kombiImage1_option){

    switch ($kombiImage1_option){
        case 0:
            $kombiImage1 = "http://sayfa.com.au/wp-content/uploads/2017/11/KOMBI-1-768x614.png";
            return $kombiImage1;
        break;

        case 1:
            $kombiImage1 = "http://sayfa.com.au/wp-content/uploads/2017/11/KOMBI-50-1-768x614.png";
            return $kombiImage1;
        break;

        case 2:
            $kombiImage1 = "http://sayfa.com.au/wp-content/uploads/2017/11/KOMBI-100-1-768x614.png";
            return $kombiImage1;
        break;
    }
    // tech drawing with stringer markings 
    //$kombiImage1 = "http://sayfa.com.au/wp-content/uploads/2017/10/kombi-calc-measurements-768x576.png";
    //return $kombiImage1;
}

function kombiImage3d(){
    // 3d image of bracket placement
    $kombiImage3d = "http://sayfa.com.au/wp-content/uploads/2017/10/kombi-calc-firstBracketMeasurement-768x576.png";
    return $kombiImage3d;
}

function validateCalcOption($opposite, $adjacent, $fixedPitch, $hypotenuse){
    If ($opposite !== null && $adjacent !== null && $fixedPitch == "" && $hypotenuse == ""){
            $calcOption = 0;
        } else if ($fixedPitch != "" && $hypotenuse != "") {
            $calcOption = 1;
        } else if ($opposite !== null && $adjacent === null){
            $calcOption = 2;
        } else {
            $calcOption = 3;
        }
    return $calcOption;
}

function calcHypotenuse($opposite, $adjacent){
    $hypotenuse = sqrt(pow($opposite, 2) + pow($adjacent, 2));
    return $hypotenuse;
}

function calcOpposite_FromAngleHypotenuse($fixedPitch, $hypotenuse){
    $opposite = $hypotenuse * sin(deg2rad($fixedPitch));
    return $opposite;
}

function calcAdjacent_FromOppositeHypotenuse($opposite, $hypotenuse){
    // TO DO: add code to calculate the adjacent from given $opposite & hypotenuse
    $adjacent = sqrt(pow($hypotenuse, 2) - pow($opposite, 2));
    return $adjacent;
}

function calcSine($opposite, $hypotenuse){
    $sin = asin($opposite / $hypotenuse);
    return $sin;
}

function calcTangent($opposite, $adjacent){
    $tan = $opposite / $adjacent;
    return $tan;
}

function calcCosine($adjacent, $hypotenuse){
    $cos = $adjacent / $hypotenuse;
    return $cos;
}

function calcTreads($opposite){
    $x = $opposite;
    $y = 1;
    WHILE ($x >= 225)
        {
            $x = $opposite / $y;
            $y++;
        }
    $treads = ($y - 2);
    return $treads;
}

function calcRise($opposite){
    $x = $opposite;
    $y = 1;
    WHILE ($x >= 225)
        {
            $x = $opposite / $y;
            $y++;
        }
    $rise = $x;
    return $rise;
}

function calcRiseWtreads($opposite, $treads){
    $x = ($treads);
    $rise = $opposite / $x;
    return $rise;
}

function calcTreadSpacing($run, $rise, $sin){
    $rise = sqrt(pow($run, 2) + pow($rise, 2));
    //$treadSpacing = $rise * (acos(rad2deg(asin($sin))));
    $treadSpacing = $rise;
    return $treadSpacing;
}

function calcFirstFrontMeasurement($rise, $treadDepth){
    $firstMeasurement = $rise - $treadDepth;
    return $firstMeasurement;
}

function calcFirstBackMeasurement($riseHypotenuse, $treadDepth){
    $firstBackMeasurement = (($riseHypotenuse - $treadDepth) - 3.5);
    return $firstBackMeasurement;
}

function calcTreadSpacingAdjacent($rise, $sin){
    $run = ($rise / tan($sin));
    return $run;
}

function calcLowerBracketAdjacent($sin){

    // Calculates the lower bracket adjacent when angle is known

    $lowerBracketAdjacent = (180 / tan($sin));
    return $lowerBracketAdjacent;
}

function calcTreadSpacingHypotenuse($rise, $run, $treadDepth){

    // Calculates the tread spacing hypotenuse

    $riseHypotenuse = sqrt(pow($rise, 2) + pow($run, 2));
    return $riseHypotenuse;
}

function calcUpperBracketAngle($sin){

    // Calculates the angle of the theoretical triangle by subtracting know sine from 90

    $upperBracketAngle = 90 - rad2deg($sin);
    return $upperBracketAngle;
}

function calcUpperBracketAdjacent($sine){

    // Uses the measurements in the array (provided by engineering department) to return the adjacent lenght of the upper bracket when angle is known
	
    $angles=array(182,179.7,177.6,175.5,174,172.4,171,169.6,168.4,167.4,166.3,165.3,164.5,163.6,162.9,162.2,161.5,160.9,160.4,159.8,159.4,158.9,158.5,158.1,157.7,157.4);
    $searchAngle = ($sine - 20);
    $upperBracketAdjacent = $angles[$searchAngle];
    return $upperBracketAdjacent;
}

function calcCutStringerLength($hypotenuse, $upperBracketAdjacent, $lowerBracketAdjacent){

    // Calculates the cut stringer lenght - Also considers endcaps at the bottom of the stringer - Due to assembly, end cap at top of the stringer isn't considered since upper bracket is attached to stringer, disregarding top end cap

    $cutStringLength = (($hypotenuse - ($upperBracketAdjacent + $lowerBracketAdjacent)) - 3.5);
    return $cutStringLength;
}

function calcFirstFrontMeasurementAdjacent($firstMeasurement, $sin){



    $firstMeasurementAdjacent = ($firstMeasurement / tan($sin));
    return $firstMeasurementAdjacent;
}

function calcFirstFrontMeasurementHypotenuse($firstMeasurement, $firstMeasurementAdjacent){

    // Calculates the theoretical triangle hypotenuse at the base of the stringer - This function also considers the 3.5 mm end cap

    $firstMeasurementHypotenuse = sqrt(pow($firstMeasurement, 2) + pow($firstMeasurementAdjacent, 2));
    return ($firstMeasurementHypotenuse) - 3.5;
}

function calcFirstFrontMeasurementHypotenuse_RoofStair($firstMeasurement, $firstMeasurementAdjacent){

    // Calculates the theoretical triangle hypotenuse at the base of the stringer

    $firstMeasurementHypotenuse = sqrt(pow($firstMeasurement, 2) + pow($firstMeasurementAdjacent, 2));
    return ($firstMeasurementHypotenuse);
}

function calcSecondTriangleHypotenuse($secondTriangleOpposite, $fixedPitch){

    // Calculates hypotenuse when Height and Angle are known - This version also accounts for the 3.5 mm Stringer End Cap

    $secondTriangleHypotenuse = ($secondTriangleOpposite / sin(deg2rad($fixedPitch)) - 3.5);
    return $secondTriangleHypotenuse;
}

function calcHeightOnlyHypotenuse($opposite, $fixedPitch){

    // Calculates Hypotenuse when Height and Angle are known

    $Hypotenuse = ($opposite / sin(deg2rad($fixedPitch)));
    return $Hypotenuse;
}

function calcKombiStairOptions($opposite){
	
    // This function is used when only the height is given.  It calculates all of the possible rises for every possible angle available for that height

    $pairs = array();

    $x = 0;
    $angle = 20; // $x <= 24 === 25 runs

        WHILE ($x <= 24){

            $treadCountTest = 1;
            
            while($treadCountTest <= 18){

                $treadRise = $opposite / $treadCountTest; // give me a tread rise //
                $treadRun = calcTreadSpacingAdjacent($treadRise, deg2rad($angle)); // gives me a tread run //
                if ($treadRise > 130 && $treadRise < 225 && $treadRun > 215 && $treadRun < 355){
                    if ((($treadRise * 2) + $treadRun) >= 540 && (($treadRise * 2) + $treadRun) <= 700) {
                        array_push($pairs, $angle, $treadCountTest);
                        }else{
                        }
                }else{
                }
                $treadCountTest = $treadCountTest + 1;
                
                }

            $x = $x + 1;
            $angle = $angle + 1;
        }

        return $pairs;
}

function calcKombiStairOptionsAngleInc($opposite, $fixedPitch){

    // This function is used when only the height and angle are known.  This is also used in the Height and Footprint calculator to calulate the rise options for the know angle.
    
    $pairs = array();

    //$x = 0;
    $angle = $fixedPitch; // $x <= 24 === 25 runs

        //WHILE ($x <= 24){

           $treadCountTest = 1;
            //$sin = deg2rad($angle);

            while($treadCountTest <= 18){

                $treadRise = $opposite / $treadCountTest; // give me a tread rise //
                $treadRun = calcTreadSpacingAdjacent($treadRise, deg2rad($angle)); // gives me a tread run //
                if ($treadRise > 130 && $treadRise < 225 && $treadRun > 215 && $treadRun < 355){
                    if ((($treadRise * 2) + $treadRun) >= 540 && (($treadRise * 2) + $treadRun) <= 700) {
                        array_push($pairs, $angle, $treadCountTest);
                        }else{
                        }
                }else{
                }
                $treadCountTest = $treadCountTest + 1;
                
                }

            //$x = $x + 1;
            //$angle = $angle + 1;
        //}

        return $pairs;
}

function validateAngle($sin){

    // Angle validate - Australian Standards require stairs to be between 20 and 45 degrees

    if(rad2deg($sin) > 45 || rad2deg($sin) < 20){
        $validated = true;
        return (bool) $validated;
    } else {
        $validated = false;
        return (bool) $validated;
    }
}	

function validateRun($run){

    // Going validate - Australian standards require a going of between 355 and 215 mm

    if ($run > 355 || $run < 215){
        $validated = true;
        return (bool) $validated;
    } else {
        $validated = false;
        return (bool) $validated;
    }
}

function validateRise($rise){

    // Rise validation - Australian standards require a rise of between 130 and 255 mm

    if ($rise > 255 || $rise < 130){
        $validated = true;
        return (bool) $validated;
    } else {
        $validated = false;
        return (bool) $validated;
    }
}

function validateTreads($treads){

    // Tread Number Validation - Australian standards allow for a maximum of 18 treads in a stairs

    if ($treads > 18) {
        $validated = true;
        return (bool) $validated;
    } else {
        $validated = false;
        return (bool) $validated;
    }
}

function validateRiseGoingCombination($rise, $run){

    // Combination Validation - Australian standards require a combination ((Rise * 2) + Run) of between 540 and 700.

    if ((($rise * 2) + $run) < 540 || (($rise * 2) + $run) > 700){
        $validated = true;
        return (bool) $validated;
    } else {
        $validated = false;
        return (bool) $validated;
    }
}

function getRedirectionURL(){

    // Used to redirect user back to calculator on error

    $redirectionURL = "http://sayfa.com.au/kombi-calculator/";
    return $redirectionURL;
}

function getErrRedirectionURL(){

    // Used to build javascript to redirect user on error

    $redirectionURL = getRedirectionURL();
    $errRedirection = "<script type='text/javascript'>window.location = '" . $redirectionURL . "'</script>";
    return $errRedirection;
}

function validatationErrMsg($errReturn, $sin, $run, $treads, $rise){

    // Error messages for each validation fail

    switch($errReturn)
    {

        case 0:
        // angle
            $err = "<script type='text/javascript'>alert('The dimensions you entered created an angle of : " . round(rad2deg($sin)) . " degrees. KOMBI stairs can only be built between 20 and 45 degrees.  Please try again.')</script>";
            return $err;
        break;

        case 1:
        // run
            $err = "<script type='text/javascript'>alert('The dimensions you entered created a tread going of : " . round($run) . " mm. KOMBI stairs can only be built a max going of 355 mm and a minimum of 215 mm.  Please try again.')</script>";
            return $err;
        break;

        case 2:
        // treads
            $err = "<script type='text/javascript'>alert('The dimensions you entered created a tread count of : " . ($treads + 2) . ". KOMBI stairs can only be built a maximum of 18 treads.  Please try again.')</script>";
            return $err;
        break;

        case 3:
        // rise
            $err = "<script type='text/javascript'>alert('The dimensions you entered created a rise of : " . round($rise) . ". KOMBI stairs can only be built a maximum rise of 255mm and a minimum of 130mm.  Please try again.')</script>";
            return $err;
        break;

        case 4:
        // rise going combination
            $err = "<script type='text/javascript'>alert('The dimensions you entered created a combination of : " . round(($rise * 2) + $run) . ", rise = $rise, run = $run, tread # = $treads KOMBI stairs can only be built a maximum combination of 700mm, or a minimum combination of 540mm.  Please try again.')</script>";
            return $err;
        break;
    }
}

/* Added 20/11/2017 to accomodate for the lenght measurement being nose to back of stairs */
function calcStairNoseToBackLenght($sin, $adjacent){

    // this is the overall theoretical triangle adjacent
    $adjacent;
    // TO DO: Calculate the lower bracket adjacent
    $lowerBracketAdjacent = calcLowerBracketAdjacent($sin);
    // TO DO: Calculate the hypotenuse
    $lowerBracketHypotenuse = sqrt(pow(180, 2) + pow($lowerBracketAdjacent, 2));
    // TO DO: Calculate the length from nose to back
    $lowerBracketToNose = pow($lowerBracketAdjacent, 2) / $lowerBracketHypotenuse;
    $length = $adjacent - $lowerBracketToNose;
    return $length; 
}

?>

﻿<?php
/**
* Template Name: KOMBI Calc Height Result Plugin
*
* @package dash
*/ ?>

<?php get_header(); ?>

<section id="content-main" role="main">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part( 'template/entry' ); ?>
    
    <?php endwhile; endif; ?>

    <!-- enter customised page code here -->

    <!-- This code is still in development.  For any issues please contact Sayfa's developer. -->
    <div>

        <!-- This is the KOMBI Stair Dimensions -->
        <div>

            <?php

            include 'KOMBI-Calc-Functions.php';

            //session_start();

            //$adjacent = $_POST['adjacent'];
            //$hypotenuse = $_POST['hypotenuse'];

            $fixedPitch = (int) $_GET['fixedPitch'];
            $opposite = $_GET['opposite'];
            $treads = $_GET['treads'];

            $treadDepth = 40;
            // Changed from 0 to 1 for testing
            // $kombiImage1_option = 0;
            // $kombiImage1 = kombiImage1($kombiImage1_option);
            // $kombiImage3d = kombiImage3d();

            $hypotenuse = calcHeightOnlyHypotenuse($opposite, $fixedPitch);
            $adjacent = calcAdjacent_FromOppositeHypotenuse($opposite, $hypotenuse);
            $sin = calcSine($opposite, $hypotenuse);
            /* Calculates the lower bracket adjacent */
            $lowerBracketAdjacent = calcLowerBracketAdjacent($sin);
            //This calculation was added on 20/11/2017 to allow for the adjusted footprint of the stair in relation to the nose of the stringer - email steven_branigan@hotmail.com for more information
            $length = calcStairNoseToBackLenght($sin, $adjacent);

            /* Calculates the upper bracket dimensions */
            $sine = $fixedPitch;
            $upperBracketAdjacent = calcUpperBracketAdjacent($sine);
            /* Calculates the cut stringer length */
            $cutStringerLength = calcCutStringerLength($hypotenuse, $upperBracketAdjacent, $lowerBracketAdjacent);
            /* calculates the tread spacing */
            $rise = calcRiseWtreads($opposite, $treads);

            /* Calculates the read rise and run */
            $run = calcTreadSpacingAdjacent($rise, $sin);

            /* Refactored as a validation duplication - 2017-10-25 */
            //if((($rise * 2) + $run) < 540 || (($rise * 2) + $run) > 700){
            //    $treads = $treads;
            //    $rise = $opposite / $treads;
            //    $run = calcTreadSpacingAdjacent($rise, $sin);
            //}else{

            //}
            $riseHypotenuse = calcTreadSpacingHypotenuse($rise, $run, $treadDepth);
            /* Calculates the tread measurements */
            $firstFrontMeasurement = calcFirstFrontMeasurement($rise, $treadDepth);
            $subsequentMeasurements = $firstFrontMeasurement + $treadSpacing;
            $firstFrontMeasurementAdjacent = calcFirstFrontMeasurementAdjacent($firstFrontMeasurement, $sin);
            $firstFrontMeasurementHypotenuse = calcFirstFrontMeasurementHypotenuse($firstFrontMeasurement, $firstFrontMeasurementAdjacent);
            $treadSpacing = calcTreadSpacing($run, $rise, $sin);

            // Added from here
            $totalMeasurement = ($firstFrontMeasurementHypotenuse - $lowerBracketAdjacent);
            //Testing echo "<p>" . $totalMeasurement . "</p>";
            //Testing echo "<p>" . $cutStringerLength . "</p>";

                        if ($totalMeasurement >= 0){
                            $totalMeasurement = $totalMeasurement;
                            $kombiImage1_option = 0;
                            //Testing echo "<p> total measurement >= 0 ";
                            //Testing echo "<p>" . $totalMeasurement . ", " . $cutStringerLength . "</p>";
                        }
                        else {

                        }
                        if ($totalMeasurement < 0 AND $totalMeasurement > -50){
                            $totalMeasurement = $totalMeasurement + 50;
                            $kombiImage1_option = 1;
                            $cutStringerLength = $cutStringerLength + 50;
                            //Testing echo "<p> total measurement < 0 ";
                            //Testing echo "<p>" . $totalMeasurement . ", " . $cutStringerLength . "</p>";
                        } else {
                            
                        }

                        if ($totalMeasurement < -50){
                            $totalMeasurement = $totalMeasurement + 100;
                            $kombiImage1_option = 2;
                            $cutStringerLength = $cutStringerLength + 100;
                            //Testing echo "<p> total measurement < 0 #2 ";
                            //Testing echo "<p>" . $totalMeasurement . ", " . $cutStringerLength . "</p>";
                        } else {
                            
                        }

                    // gets the correct images for the display results page
                    $kombiImage1 = kombiImage1($kombiImage1_option);
                    $kombiImage3d = kombiImage3d();

            // to here

            if (validateAngle($sin)){
                $err = validatationErrMsg(0, $sin, $run, $treads, $rise);
                echo "$err";
                $redirect = getErrRedirectionURL();
                echo "$redirect";
            } else if (validateRun($run)){
                $err = validatationErrMsg(1, $sin, $run, $treads, $rise);
                echo "$err";
                $redirect = getErrRedirectionURL();
                echo "$redirect";
            } else if (validateTreads($treads)){
                $err = validatationErrMsg(2, $sin, $run, $treads, $rise);
                echo "$err";
                $redirect = getErrRedirectionURL();
                echo "$redirect";
            } else if (validateRise($rise)){
                $err = validatationErrMsg(3, $sin, $run, $treads, $rise);
                echo "$err";
                $redirect = getErrRedirectionURL();
                echo "$redirect";
            } else if (validateRiseGoingCombination($rise, $run)){
                $err = validatationErrMsg(4, $sin, $run, $treads, $rise);
                echo "$err";
                $redirect = getErrRedirectionURL();
                echo "$redirect";
            } else {
            
                echo "<div>";

                echo "<div style='float:right;'>";
                    echo "<img src='" . $kombiImage1 . "' style='max-width:100%; height:auto;' alt='KOMBI Stair Markings Instructions'>";
                echo "</div>";

                echo "<div style='float:left; width:300px'>";
                    
                    echo "<h3>KOMBI STAIR DIMENSIONS TEST</h3>";
                    // Prints table with main information related to the stair
                    echo "<table>";
                            echo "<tr><td><strong>(H) Height: </strong></td><td>" . round($opposite) . " mm</td></tr>";
                            echo "<tr><td><strong>(FP) Footprint: </strong></td><td>" . round($length) . " mm</td></tr>";
                            echo "<tr><td><strong>(L) Length: </strong></td><td>" . round($cutStringerLength) . " mm</td></tr>";
                            echo "<tr><td><strong> Tread Count</strong>: </td><td>" . ($treads - 1) . "</td></tr>";
                            echo "<tr><td><strong>(A) Stair Angle: </td><td>" . round($fixedPitch) . "°</td></tr>";
                            echo "<tr><td><strong>(R) Rise:</strong></td><td>" . round($rise) . " mm</td></tr>";
                            echo "<tr><td><strong>(G) Going:</strong></td><td>" . round($run) . " mm</td></tr>";
                    echo "</table>";

                    echo "<img src='" . $kombiImage3d . "' style='max-width:100%; height:auto;' alt='KOMBI Stair 3D Profile'><br>";

                echo "</div>";

                echo "<div style='width:100%;'>";

                    //$totalMeasurement = ($firstFrontMeasurementHypotenuse - $lowerBracketAdjacent);

                    $totalMeasurementCount = 1;
                    echo "<table>";
                    echo "<tr><td colspan='3'><h3>TREAD MEASUREMENTS</h3></td></tr>";
                    echo "<tr><td><strong>TREAD</strong></td><td><strong>FRONT MARKING</strong></td><td><strong>BACK MARKING</strong></td></tr>";
                    echo "<tr><td>1</td><td style='color:red;'><strong>F1 = " . round($totalMeasurement) . " mm</strong></td><td style='color:blue;'><strong>B1 = " . round($firstFrontMeasurementHypotenuse) . " mm</strong></td></tr>";

                        $backMarking = ($firstFrontMeasurementHypotenuse);
                        WHILE ($totalMeasurementCount < ($treads - 1))
                            {
                                $backMarking = ($backMarking + $riseHypotenuse);
                                $totalMeasurement = ($totalMeasurement + $treadSpacing);
                                //echo "<tr><td>" . ($totalMeasurementCount + 1) . "</td><td style='color:red;'><strong>F" . ($totalMeasurementCount + 1) . " = " . round($totalMeasurement) . " mm </strong></td><td style='color:blue;'><strong>F" . ($totalMeasurementCount + 1) . " = " . round($backMarking) . "</strong></td></tr>";
                                echo "<tr><td>" . ($totalMeasurementCount + 1) . "</td><td style='color:red;'><strong>F" . ($totalMeasurementCount + 1) . " = " . round($totalMeasurement) . " mm </strong></td><td style='color:blue;'><strong>B" . round($totalMeasurementCount + 1) . " = " . round($backMarking) . " mm</strong></td></tr>";
                                $totalMeasurementCount++;
                            }

                echo "</table>";
                echo "</div>";
            }						

            ?>

        </div>

    </div>

    <!-- to here -->
</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>

<?php
/**
* Template Name: KOMBI Calculator Options Plugin
*
 *
* @package dash
*/ ?>

<?php get_header(); ?>

<section id="content-main" role="main">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part( 'template/entry' ); ?>
    
    <?php endwhile; endif; ?>

        <!-- enter customised page code here -->

        <div style="clear:left;">

            <!-- This is the KOMBI Stair Dimensions -->
            <div>

            <?php

                /*
                    includes the KOMBI-CALC-FUNCTIONS.php file, which contains all the calculator functions;
                     - calcHypotenuse()
                     - calcSine()
                */
                include 'KOMBI-Calc-Functions.php';
                //include 'kombi-settings-form.php';

                echo "<style media='screen' type='text/css'> td.kombiCell { background-color: #1a206d } td.kombiCell:hover { background-color: #adb1eb } </style>";
                echo "<style media='screen' type='text/css'> td.kombiCellGreen { background-color: #009900 } td.kombiCellGreen:hover { background-color: #66ff66 } </style>";

                //session_start();

                // Variables passed from the calculator form
                $opposite = $_POST['opposite'];
                $adjacent = $_POST['adjacent'];
                $fixedPitch = $_POST['fixedPitch'];
                $hypotenuse = $_POST['hypotenuse'];

                $treadDepth = 40;
                //$kombiImage1 = kombiImage1();
                //$kombiImage3d = kombiImage3d();

                /* 
                    Assesses which calculator is required based on the values in each of the declared variables, - $opposite, - $adjacent, - $fixedPitch, - $hyptonuse
                */

                $calcOption = validateCalcOption($opposite, $adjacent, $fixedPitch, $hypotenuse);

                /*
                    Switch Statement controls the output of the calculator

                    If the calculator $calcOption is set to 1, the code in case 1 runs, if $calcOption == 2, the code in case 2 runs, etc.
                    The $calcOption variable is set above
                */
                switch ($calcOption)
                {   
                
                    case 0:
                    // KOMBI Height & Footprint Options Calculator

                    if (isset($_POST['kombi_height_results'])) {
                        update_option('kombi_height_results', $_POST['kombi_height_results']);
                        $kombi_roof_results = $_POST['kombi_height_results'];
                    } 

                    $kombi_height_results = get_option('kombi_height_results', 'not set');

                    echo '<script type="text/javascript">
                            function myTableCellSelect(e){ 
                            var test = e.target.innerText;
                            var res = test.split(" ");
                            var url = "' . $kombi_height_results .  '/?opposite=' . $opposite . '&fixedPitch=";
                            var url2 = url+res[0]+"&treads="+res[1];
                            window.location = "" + url2;
                            }
                            </script>';

                    /* The following calls to the KOMBI-CALC-FUNCTIONS.php file are the calculations required for the KOMBI stairs */

                    /* Calculates the overall right angle triangle dimensions */
                    $hypotenuse = calcHypotenuse($opposite, $adjacent);
                    $sin = calcSine($opposite, $hypotenuse);

                    /* Calculates the lower bracket adjacent */
                    $lowerBracketAdjacent = calcLowerBracketAdjacent($sin);
                    /* Calculates the upper bracket dimensions */
                    $sine = round(rad2deg($sin));
                    $upperBracketAdjacent = calcUpperBracketAdjacent($sine);
                    /* Calculates the cut stringer length */
                    $cutStringerLength = calcCutStringerLength($hypotenuse, $upperBracketAdjacent, $lowerBracketAdjacent);
                    /* Calculates the required number of treads */
                    $treads = calcTreads($opposite);
                    /* calculates the tread spacing */
                    $rise = calcRise($opposite);
                    /* Calculates the read rise and run */
                    $run = calcTreadSpacingAdjacent($rise, $sin);

                    $pairs = calcKombiStairOptionsAngleInc($opposite, $sine);
                    //print_r($pairs);
                    $treadOptions = array();
                    $angleOptions = array();


                    $result = count($pairs);
                    //echo $result;
                    /*
                    This array is a combination of the 1st and 2nd index of the $pairs array as a singe index.  E.g. $pairs([0] 30, [1]) 15 will become $pairs2([0]30, 15)
                    This array will be used to confirm if the value of the matrix table cell is in the array, 
                    if it is it will turn the cell green and hide the values in the cell
                    so that they can be used in the calculator
                    */
                    $pairsPairs = array();

                    $x = 0;
                    $sb = 0;

                    for($x = 0; $x <= $result; $x++){
                        $y = $x + 1;
                        $pair = "" . $pairs[$x] . ", " . $pairs[$y] . "";
                        array_push($pairsPairs, $pair);
                        $x++;
                        $sb = $sb + 1;
                    }

                    // Builds the table for the options
                    $x = 0;
                    $i = 0; 
                    $j = 1; 

                    while ($x < $result){
                        array_push($angleOptions, $pairs[$i]);
                        array_push($treadOptions, $pairs[$j]);

                        $i = $i + 2;
                        $j = $j + 2;
                        $x = $x + 2;
                    }

                    $treadOptions2 = array_reverse(array_values(array_unique($treadOptions)));
                    // print_r($treadOptions2);
                    $angleOptions2 = array_reverse(array_values(array_unique($angleOptions)));
                    // print_r($angleOptions2);*/

                    $tableCellMatrixPairs = array();

                    foreach ($angleOptions2 as $value){
                        for($x = 0; $x <= count($treadOptions2) - 1; $x++){
                            $pair = "" . $value . ", " .  $treadOptions2[$x];
                            array_push($tableCellMatrixPairs, $pair);
                        }
                    }

                    $s = 0;
                    $b = 0;
                    echo "<div style='padding: 30px; overflow-x: auto; white-space: nowrap;'>";
                    echo "<table id='tableID' onclick='myTableCellSelect(event)' style='width:100%;'>";
                    echo "<tr>";
                    echo "<td><strong>Angle / Rises</strong></td>";
                    // tr td { background-color: white } tr:hover td { background-color: black };
                    foreach($treadOptions2 as $value){
                        echo "<td align='center'><strong>".$value."</strong></td>";
                    }
                    echo "</tr>";

                    foreach($angleOptions2 as $value){
                        echo "<tr><td><strong>".$value."°</strong></td>";

                            for($x = 0; $x <= count($treadOptions2) - 1; $x++){

                                $pair = "" . $value . ", " . $treadOptions2[$x];
                                    if(in_array($pair, $pairsPairs)){
                                        echo "<td class='kombiCell' align='center'><font color='#ffffff'><strong>$value" . "°   $treadOptions2[$x]</strong></td>";
                                    } else {
                                        echo "<td></td>";
                                    }

                                }

                            $s = 0;
                            $s = 0;
                            $b = $b + 1;

                        echo "</tr>";

                    }

                    echo "</table>";
                    echo "</div>";
                    break;
                    
                // Case 1 = Angle & Hypotenuse
                case 1: 

                    if (isset($_POST['kombi_roof_results'])) {
                        update_option('kombi_roof_results', $_POST['kombi_roof_results']);
                        $kombi_roof_results = $_POST['kombi_roof_results'];
                    } 

                    $kombi_roof_results = get_option('kombi_roof_results', 'not set');

                    $opposite = calcOpposite_FromAngleHypotenuse($fixedPitch, $hypotenuse);

                    echo '<script type="text/javascript">
                            function myTableCellSelect(e){ 
                            var test = e.target.innerText;
                            var res = test.split(" ");
                            var url = "' . $kombi_roof_results . '/?hypotenuse=' . $hypotenuse  . '&fixedPitch=";
                            var url2 = url+res[0]+"&treads="+res[1];
                            window.location = "" + url2;
                            }
                            </script>';

                    //$opposite = calcOpposite_FromAngleHypotenuse($fixedPitch, $hypotenuse);
                    //echo "Opposite: " . round($opposite);

                    $pairs = calcKombiStairOptionsAngleInc($opposite, $fixedPitch);
                    //print_r($pairs);
                    $treadOptions = array();
                    $angleOptions = array();


                    $result = count($pairs);
                    //echo $result;
                    /*
                    This array is a combination of the 1st and 2nd index of the $pairs array as a singe index.  E.g. $pairs([0] 30, [1]) 15 will become $pairs2([0]30, 15)
                    This array will be used to confirm if the value of the matrix table cell is in the array, 
                    if it is it will turn the cell green and hide the values in the cell
                    so that they can be used in the calculator
                    */
                    $pairsPairs = array();

                    $x = 0;
                    $sb = 0;

                    for($x = 0; $x <= $result; $x++){
                        $y = $x + 1;
                        $pair = "" . $pairs[$x] . ", " . $pairs[$y] . "";
                        array_push($pairsPairs, $pair);
                        $x++;
                        $sb = $sb + 1;
                    }

                    // Builds the table for the options
                    $x = 0;
                    $i = 0; 
                    $j = 1; 

                    while ($x < $result){
                        array_push($angleOptions, $pairs[$i]);
                        array_push($treadOptions, $pairs[$j]);

                        $i = $i + 2;
                        $j = $j + 2;
                        $x = $x + 2;
                    }

                    $treadOptions2 = array_reverse(array_values(array_unique($treadOptions)));
                    $angleOptions2 = array_reverse(array_values(array_unique($angleOptions)));

                    $tableCellMatrixPairs = array();

                    foreach ($angleOptions2 as $value){
                        for($x = 0; $x <= count($treadOptions2) - 1; $x++){
                            $pair = "" . $value . ", " .  $treadOptions2[$x];
                            array_push($tableCellMatrixPairs, $pair);
                        }
                    }

                    $s = 0;
                    $b = 0;
                    echo "<div style='padding: 30px; overflow-x: auto; white-space: nowrap;'>";
                    echo "<table id='tableID' onclick='myTableCellSelect(event)' style='width:100%;'>";
                    echo "<tr>";
                    echo "<td><strong>Angle / Rises</strong></td>";

                    foreach($treadOptions2 as $value){
                        echo "<td align='center'><strong>".$value."</strong></td>";
                    }
                    echo "</tr>";

                    foreach($angleOptions2 as $value){
                        echo "<tr><td><strong>".$value."°</strong></td>";

                            for($x = 0; $x <= count($treadOptions2) - 1; $x++){

                                $pair = "" . $value . ", " . $treadOptions2[$x];
                                    if(in_array($pair, $pairsPairs)){
                                        echo "<td class='kombiCell' align='center'><font color='#ffffff'><strong>$value" . "°   $treadOptions2[$x]</strong></td>";
                                    } else {
                                        echo "<td></td>";
                                    }

                                }

                            $s = 0;
                            $s = 0;
                            $b = $b + 1;

                        echo "</tr>";

                    }

                    echo "</table>";
                    echo "</div>";
                    break;
                    
                case 2:

                     if (isset($_POST['kombi_height_results'])) {
                        update_option('kombi_height_results', $_POST['kombi_height_results']);
                        $kombi_height_results = $_POST['kombi_height_results'];
                    } 

                    $kombi_height_results = get_option('kombi_height_results', 'not set');

                    echo '<script type="text/javascript">
                            function myTableCellSelect(e){ 
                            var test = e.target.innerText;
                            var res = test.split(" ");
                            var url = "' . $kombi_height_results . '/?opposite=' . $opposite . '&fixedPitch=";
                            var url2 = url+res[0]+"&treads="+res[1];
                            window.location = "" + url2;
                            }
                            </script>';

                    $pairs = calcKombiStairOptions($opposite);
                    $treadOptions = array();
                    $angleOptions = array();

                    $result = count($pairs);

                    /*
                    This array is a combination of the 1st and 2nd index of the $pairs array as a singe index.  E.g. $pairs([0] 30, [1]) 15 will become $pairs2([0]30, 15)
                    This array will be used to confirm if the value of the matrix table cell is in the array, 
                    if it is it will turn the cell green and hide the values in the cell
                    so that they can be used in the calculator
                    */
                    $pairsPairs = array();

                    $x = 0;
                    $sb = 0;

                    for($x = 0; $x <= $result; $x++){
                        $y = $x + 1;
                        $pair = "" . $pairs[$x] . ", " . $pairs[$y] . "";
                        array_push($pairsPairs, $pair);
                        $x++;
                        $sb = $sb + 1;
                    }

                    // Builds the table for the options
                    $x = 0;
                    $i = 0; 
                    $j = 1; 

                    while ($x < $result){
                        array_push($angleOptions, $pairs[$i]);
                        array_push($treadOptions, $pairs[$j]);

                        $i = $i + 2;
                        $j = $j + 2;
                        $x = $x + 2;
                    }

                    $treadOptions2 = array_reverse(array_values(array_unique($treadOptions)));
                    $angleOptions2 = array_reverse(array_values(array_unique($angleOptions)));

                    $tableCellMatrixPairs = array();

                    foreach ($angleOptions2 as $value){
                        for($x = 0; $x <= count($treadOptions2) - 1; $x++){
                            $pair = "" . $value . ", " .  $treadOptions2[$x];
                            array_push($tableCellMatrixPairs, $pair);
                        }
                    }

                    $s = 0;
                    $b = 0;
                    echo "<div style='padding: 30px; overflow-x: auto; white-space: nowrap;'>";
                    echo "<table id='tableID' onclick='myTableCellSelect(event)' style='width:100%;'>";
                    echo "<tr>";
                    echo "<td><strong>Angle / Rises</strong></td>";

                    foreach($treadOptions2 as $value){
                        echo "<td align='center'><strong>".$value."</strong></td>";
                    }
                    echo "</tr>";

                    foreach($angleOptions2 as $value){
                        echo "<tr><td><strong>".$value."°</strong></td>";

                            for($x = 0; $x <= count($treadOptions2) - 1; $x++){

                                $pair = "" . $value . ", " . $treadOptions2[$x];
                                    if(in_array($pair, $pairsPairs)){
                                        if ($value != 40){
                                            echo "<td class='kombiCell' align='center'><font color='#ffffff'><strong>$value" . "°   $treadOptions2[$x]</strong></td>";
                                        } 
                                        else{
                                            echo "<td class='kombiCellGreen' align='center'><font color='#ffffff'><strong>$value" . "°   $treadOptions2[$x]</strong></td>";
                                        }
                                        
                                    } else {
                                        echo "<td></td>";
                                    }

                                }

                            $s = 0;
                            $s = 0;
                            $b = $b + 1;

                        echo "</tr>";

                    }

                    echo "</table>";
                    echo "</div>";
                    break;

                }

            ?>

            </div>


        </div>

        <!-- to here -->
</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>

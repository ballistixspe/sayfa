<?php
//if ($instance['new_window']) $additional_attribute = ' target="_blank"';

if($instance['class']) echo'<div class="'.$instance['class'].'">';

if($instance['block_link']) echo '<a href=' . sow_esc_url($instance['url'] ) . $additional_attribute . '>';

if($instance['image']) printf('<div class="image">%1$s</div>',wp_get_attachment_image($instance['image'] , $instance['image_size']));

if($instance['title']) printf('<div class="title"><h3>%1$s</h3></div>', $instance['title']);

if($instance['text']) printf('<div class="content">%1$s</div>', wpautop($instance['text']));

//if( !($instance['block_link'])) echo '<a class="link-button" href=' . sow_esc_url($instance['url'])  . $additional_attribute . '>' . $instance['link_button_text'] . '</a>';

if($instance['block_link']) echo '</a>';

echo '</div>';
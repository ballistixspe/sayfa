<?php

/*
Widget Name: Sayfa Card
Description: Custom Card Widget for Sayfa
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class SAYFA_CARD_WIDGET extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'sayfa-card-widget',
	        __('Sayfa Card Widget', 'sayfa-card-widget-text-domain'),
	        array(
	            'description' => __('Custom Card Widget for Sayfa.', 'card-widget-text-domain'),
	            'help'        => 'http://example.com/card-widget-docs',
	        ),
	        array(),
			array(
                'class' => array(
                    'type' => 'select',
                    'label' => __( 'Choose a class', 'widget-form-fields-text-domain' ),
                    'default' => 'class_one',
                    'options' => array(
                        'class_one' => __( 'Class One', 'widget-form-fields-text-domain' ),
                        'class_two' => __( 'Class Two', 'widget-form-fields-text-domain' )
                    )
                ),
            	'title' => array(
            	    'type' => 'text',
            	    'label' => __('Title.', 'widget-form-fields-text-domain')
            	) ,
				'image' => array(
				    'type' => 'media',
				    'label' => __( 'Choose an image.', 'widget-form-fields-text-domain' ),
				    'choose' => __( 'Choose image', 'widget-form-fields-text-domain' ),
				    'update' => __( 'Set image', 'widget-form-fields-text-domain' ),
				    'library' => 'image',
				    'fallback' => true
				),
                'image_size' => array(
                    'type' => 'text',
                    'default' => 'medium',
                    'label' => __('Image Size', 'widget-form-fields-text-domain')
                ) ,
                'text' => array(
                    'type' => 'tinymce',
                    'label' => __( 'Content', 'widget-form-fields-text-domain' ),
                    //'default' => 'An example of a long message.</br>It is even possible to add a few html tags.</br><a href="siteorigin.com" target="_blank"">Links!</a>',
                    'rows' => 5,
                    'default_editor' => 'html',
                    'button_filters' => array(
                        'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                        'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                        'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                        'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                        'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                    ),
                ),

                'block_link' => array(
                    'type' => 'checkbox',
                    'label' => __('Make the whole card as link', 'so-widgets-bundle'),
                    //'default' => false,
                    'state_emitter' => array(
                        'callback' => 'conditional',
                        'args' => array(
                            'group[active]: val',
                            'group[inactive]: !val',
                        )
                    ),
                ),

                'url' => array(
                    'type' => 'link',
                    'label' => __('Destination URL', 'so-widgets-bundle'),
                    'state_handler' => array(
                        'group[active]' => array('show'),
                        'group[inactive]' => array('hide'),
                    ),
                ),
                'new_window' => array(
                    'type' => 'checkbox',
                    'label' => __( 'Open link in a new window', 'widget-form-fields-text-domain' ),
                    'default' => false,
                    'state_handler' => array(
                        'group[active]' => array('show'),
                        'group[inactive]' => array('hide'),
                    ),
                ),
                
                // 'link_button_text' => array(
                //     'type' => 'text',
                //     'default' => 'Read More',
                //     'label' => __('Link Button Text', 'widget-form-fields-text-domain')
                // ) ,
            
	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

    function enqueue_frontend_scripts( $instance ) {

        wp_enqueue_style(
            'sayfa-card-widget',
            plugin_dir_url(__FILE__).'styles/style.css'
        );

        parent::enqueue_frontend_scripts( $instance );
    }
    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return '';
    }
}

siteorigin_widget_register('sayfa-card-widget', __FILE__, 'SAYFA_CARD_WIDGET');

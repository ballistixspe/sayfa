<?php

global $post;

$current_page = $post->ID;

// WP_Query arguments
$args_side_menu = array (
	'post_type'				=> 'page',
	'post_parent'            => $instance['parent'],
	'posts_per_page'         => '-1',
	'order'                  => 'ASC',
	'orderby'                => 'menu_order',
);

// The Query
$query_side_menu = new WP_Query( $args_side_menu );

// The Loop
if ( $query_side_menu->have_posts() ) {
	echo '<nav class="navigation-sidebar"><ul class="menu">';
	while ( $query_side_menu->have_posts() ) {
		$query_side_menu->the_post();
		?>
		<li class="<?php if( $current_page === get_the_id() ) echo 'current-menu-item'; ?>">
			<a href="<?php the_permalink(); ?>">
				<?php
				$image = get_field('icon');

				if( !empty($image) ): ?>
					<span class="icon">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</span>
				<?php endif; ?>

				<?php the_title(); ?>
			</a>
		</li>
		<?php
	}
	echo '</ul></nav>';
} else {
	// no posts found
	echo "null";
}

// Restore original Post Data
wp_reset_postdata();

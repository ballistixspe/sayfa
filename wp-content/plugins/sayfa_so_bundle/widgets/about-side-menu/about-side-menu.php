<?php

/*
Widget Name: About Side Menu
Description: A about side menu Sayfa Exlusive
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class About_Side_Menu_Widget extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'about-side-menu-widget',
	        __('About Side Menu Widget', 'about-side-menu-widget-text-domain'),
	        array(
	            'description' => __('An about side menu widget for Sayfa.', 'about-side-menu-widget-text-domain'),
	            'help'        => 'http://example.com/about-side-menu-widget-docs',
	        ),
	        array(),
			array(
				'class' => array(
	                'type' => 'text',
	                'label' => __('Class', 'widget-form-fields-text-domain')
	            ),
	            'parent' => array(
			        'type' => 'text',
			        'label' => __('Parent', 'widget-form-fields-text-domain'),
			    )
	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return '';
    }
}

siteorigin_widget_register('about-side-menu-widget', __FILE__, 'About_Side_Menu_Widget');

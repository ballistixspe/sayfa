<?php


$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
?>


<?php
$terms = get_terms( 'product_category', array( 'parent' => 0, 'hide_empty'=> false) );
$parents = array();
foreach ($terms as $term) {
	$order= get_field('order',  'product_category_' . $term->term_id );
	$parents[$order] = $term;
}
ksort( $parents, SORT_NUMERIC ) ;
?>

<nav class="navigation-sidebar <?php echo $instance['class']; ?>">
	<ul class="menu">
	<?php
	foreach ($parents as $parent) {

		$terms = get_terms( 'product_category', array( 'parent' => $parent->term_id, 'hide_empty'=> false) );

		$children = array();
		foreach ($terms as $term) {
			$order= get_field('order',  'product_category_' . $term->term_id );
			$children[$order] = $term;
		}
		ksort( $children, SORT_NUMERIC ) ;

		foreach ($children as $child) {

			$icon = get_field('icon',  $child->taxonomy ."_" .$child->term_id );
			
			?>
			<li class="<?php if( $term_id === $child->term_id ) echo 'current-menu-item'; ?>">
				<a class="<?php echo $parent->slug; ?>"
				href="<?php echo get_term_link($child->term_id); ?>"
				title="<?php sprintf( __( 'View all post filed under %s', '_sayfa_domain' ), $child->name ); ?>">
					
					<?php if( $icon['url']) { 
						echo '<span class="icon"><img src="' . $icon['url'] . '" alt=""></span>';
					} ?>
					
					<span class="title"><?php echo $child->name; ?></span>
				</a>
			</li>
			<?php
		}
	} ?>
	</ul>
</nav>

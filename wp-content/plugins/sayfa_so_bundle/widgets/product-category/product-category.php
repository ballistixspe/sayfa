<?php

/*
Widget Name: Product Category
Description: A product category Sayfa Exlusive
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class Product_Category_Widget extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'product-category-widget',
	        __('Product Category Widget', 'product-category-widget-text-domain'),
	        array(
	            'description' => __('A product category widget for Sayfa.', 'product-category-widget-text-domain'),
	            'help'        => 'http://example.com/product-category-widget-docs',
	        ),
	        array(),
			array(
				'class' => array(
	                'type' => 'text',
	                'label' => __('Class', 'widget-form-fields-text-domain')
	            )
	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return '';
    }
}

siteorigin_widget_register('product-category-widget', __FILE__, 'Product_Category_Widget');

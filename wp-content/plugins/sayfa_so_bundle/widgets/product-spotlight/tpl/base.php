<?php
if ( ! empty( $instance['slides'] ) ) :
$slides = $instance['slides'];
echo '<div id="product-spotlight" class="owl-carousel">';

foreach ( $slides as $index => $slide ) :
  $image =  wp_get_attachment_image_src( $slide['image'] , 'full' );
  printf('<div class="flip-container" ontouchstart="this.classList.toggle("hover");"><div class="flipper"><div class="front"><img src="%1$s"></div><div class="back"><div class="excerpt">%2$s</div></div></div></div>', $image[0], $slide['excerpt']);
endforeach;
echo '</div>';
endif; ?>

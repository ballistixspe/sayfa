<?php                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               /*
Widget Name: Spotlight Carousel
Description: A spotlight carousel Sayfa Exlusive
Author: Marcel Badua
Author URI: http://example.com
Widget URI: http://example.com/hello-world-widget-docs,
Video URI: http://example.com/hello-world-widget-video
*/

class Spotlight_Carousel_Widget extends SiteOrigin_Widget {

	function __construct() {
	    parent::__construct(
	        'spotlight-carousel-widget',
	        __('Spotlight Carousel Widget', 'spotlight-carousel-widget-text-domain'),
	        array(
	            'description' => __('A spotlight carousel widget for Sayfa.', 'spotlight-carousel-widget-text-domain'),
	            'help'        => 'http://example.com/spotlight-carousel-widget-docs',
	        ),
	        array(),
			array(
                'slides' => array(
                    'type' => 'repeater',
                    'label' => __( 'Slides.' , 'widget-form-fields-text-domain' ),
                    'item_name'  => __( 'Slide', 'siteorigin-widgets' ),
                    'fields' => array(

						'image' => array(
						    'type' => 'media',
						    'label' => __( 'Choose a background image.', 'widget-form-fields-text-domain' ),
						    'choose' => __( 'Choose image', 'widget-form-fields-text-domain' ),
						    'update' => __( 'Set image', 'widget-form-fields-text-domain' ),
						    'library' => 'image',
						    'fallback' => true
						),
			            'url' => array(
			            	'type' => 'link',
			            	'label' => __('Destination URL', 'so-widgets-bundle'),
			            ),
			            'excerpt' => array(
			                'type' => 'tinymce',
			                'label' => __( 'Excerpt', 'widget-form-fields-text-domain' ),
			                'default' => 'An example of a long message.</br>It is even possible to add a few html tags.</br><a href="siteorigin.com" target="_blank"">Links!</a>',
			                'rows' => 10,
			                'default_editor' => 'html',
			                'button_filters' => array(
			                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
			                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
			                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
			                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
			                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
			                ),
			            ),

                    )
                ),
	        ),
	        plugin_dir_path(__FILE__)
	    );
	}

	/**
	 * Initialize the CTA widget
	 */
	function initialize(){

	    $this->register_frontend_styles(
	        array(
	            array(
	                'owl.Carousel',
	                'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css',
	            )
	        )
	    );

	    $this->register_frontend_scripts(
	        array(
	        	array(
	                'owl.Carousel',
	                'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js',
	                array( 'jquery' ),
	                '1.2.0',
	                TRUE
	            ),
	            array(
	                'product-spotlight',
	                plugin_dir_url(__FILE__) . 'js/product-spotlight.js',
	                array( 'jquery', 'owl.Carousel' ),
	                '1.2.0',
	                TRUE
	            ),
	        )
	    );
	}
    function get_template_name($instance) {
        return 'base';
    }

    function get_style_name($instance) {
        return '';
    }
}

siteorigin_widget_register('spotlight-carousel-widget', __FILE__, 'Spotlight_Carousel_Widget');

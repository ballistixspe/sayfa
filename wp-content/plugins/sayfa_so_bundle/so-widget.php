<?php
/**
* Plugin Name: Sayfa: SiteOrigin Widget Bundle
* Description: A collection of all widgets created for Bootstrap.
* Plugin URI: http://#
* Author: Marcel Badua
* Author URI: http://#
* Version: 1.0
* License: GPL2
* Text Domain: Text Domain
* Domain Path: Domain Path
*/

/*
Copyright (C) 2015  Marcel Badua  bitterdash@gmail.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'plugins_loaded', array( 'SAYFA_SO_BUNDLE', 'get_instance' ) );
register_activation_hook( __FILE__, array( 'SAYFA_SO_BUNDLE', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'SAYFA_SO_BUNDLE', 'deactivate' ) );

class SAYFA_SO_BUNDLE {

	private static $instance = null;

	public static function get_instance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		
		add_filter('siteorigin_widgets_widget_folders', array(&$this, 'sayfa_so_bundle_collection'));

	}

	public function sayfa_so_bundle_collection($folders){
    	
    	$folders[] = dirname(__FILE__) . '/widgets/';

    	return $folders;
	}

	public static function activate() {}

	public static function deactivate() {}

/*
	public static function uninstall() {
		if ( __FILE__ != WP_UNINSTALL_PLUGIN )
			return;
	}
*/
}
<?php

/*
Plugin Name: Sayfa: Products
Plugin URI: http://ballistix.com/
Description: SAYFA_PRODUCTS
Version: 1.0
Author: Ballistix SPE
Author URI: http://ballistix.com/
License: GPL2
*/

/*
Copyright 2015  Marcel Badua  (email : marcel.badua@ballistix.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if (!class_exists('SAYFA_PRODUCTS')) {
    class SAYFA_PRODUCTS
    {
        /**
         * Construct the plugin object
         */
        public function __construct() {

            require_once( sprintf("%s/inc/shortcode.php", dirname(__FILE__)) );

            require_once( sprintf("%s/inc/actions.php", dirname(__FILE__)) );

            require_once( sprintf("%s/inc/widget.php", dirname(__FILE__)) );
            $SAYFA_PRODUCTS_WIDGET = new SAYFA_PRODUCTS_WIDGET();

            require_once (sprintf("%s/inc/construct.php", dirname(__FILE__)));
            $SAYFA_PRODUCTS_CONSTRUCT = new SAYFA_PRODUCTS_CONSTRUCT();


        } // END public function __construct

        /**
         * Activate the plugin
         */
        public static function activate() {
            // Do nothing
        } // END public static function activate
        
        /**
         * Deactivate the plugin
         */
        public static function deactivate() {
            // Do nothing
        }// END public static function deactivate

    } // END class SAYFA_PRODUCTS
} // END if(!class_exists('SAYFA_PRODUCTS'))

if (class_exists('SAYFA_PRODUCTS')) {
    // Installation and uninstallation hooks
    register_activation_hook(__FILE__, array('SAYFA_PRODUCTS','activate'));
    register_deactivation_hook(__FILE__, array( 'SAYFA_PRODUCTS', 'deactivate'));
    // instantiate the plugin class
    $wp_plugin_template = new SAYFA_PRODUCTS();
}

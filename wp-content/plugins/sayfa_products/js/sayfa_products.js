(function($) {
    $(".fancybox").fancybox();
    // $('[href*="/product/"]').addClass('product_modal');
    //$('body').on('click', '#loop-products a.entry, #virtual-showroom a, a.product_modal', function(event) {
    $('body').on('click', 'a.product_modal, #virtual-showroom a', function(event) {
        var data = {
            action: 'sayfa_get_product',
            nonce: sayfa_products.nonce,
            value: $(this).attr('href')
        };
        $.ajax({
            type: 'post',
            url: sayfa_products.url,
            cache: false,
            data: data,
            beforeSend: function() {
                $("#productModal").modal('show');
                $("#productModal .modal-title").html('');
                $("#productModal .modal-body").html('<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>');
            },
            success: function(response) {
                $("#productModal .modal-title").html(response.data.title);
                $("#productModal .modal-body").html(response.data.content);
            }
        });
        return false;
    });
    $(document).on('click', '.gallery-thumbnail', function(e) {
        e.preventDefault();
        $("#image-placeholder").html('<i class="fa fa-spinner fa-spin fa-4x"></i>').addClass('loading');
        var newSrc = $(this).data('full'),
            fullImage = $(this).data('full'),
            image = new Image();
        image.onload = function() {
            $("#image-placeholder").removeClass('loading').html('<img class="img-responsive" />');
            $("#image-placeholder").attr("href", fullImage);
            $("#image-placeholder img").attr("src", newSrc);
        }
        image.src = newSrc;
    });
})(jQuery);

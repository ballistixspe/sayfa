(function($) {

    
    
    $(document).on('keyup', '#find-product-category', function(event) {
        var data = {
            action: 'sayfa_find_product_category',
            nonce: sayfa_products.nonce,
            data: {
                value: $(this).val(),
                taxonomy: $(this).attr('data-taxonomy')
            }
        };
        $.ajax({
            type: 'post',
            url: sayfa_products.url,
            cache: false,
            data: data,
            beforeSend: function() {
                $("#loop-products").html('<i class="fa fa-spinner fa-spin fa-4x"></i>');
            },
            success: function(response) {
                $("#loop-products").hide().html(response.data).fadeIn();
            }
        });
        return false;
    });

    // $(document).on('click', '#productModalToggle', function(event) {
    //     var data = {
    //         action: 'sayfa_products_toggle',
    //         nonce: sayfa_products_widget.nonce,
    //         value: $(this).data('value'),
    //         title: $(this).data('title')
    //     };
    //     $.ajax({
    //         type: 'post',
    //         url: sayfa_products_widget.url,
    //         cache: false,
    //         data: data,
    //         beforeSend: function() {
    //             $("#productModal .modal-body").html('<i class="fa fa-spinner fa-spin fa-4x"></i>').addClass('loading'); //.html('<i class="fa fa-spinner fa-spin fa-4x"></i>');
    //             $("#productModal .modal-title").html( data.title );
    //         },
    //         success: function(response) {
    //             //$("#productModal .modal-body").hide().html(response.data).fadeIn();
    //             $("#productModal .modal-body").removeClass('loading').html(response.data);
    //         }
    //     });
    //     return false;
    // });

    
    
})(jQuery);
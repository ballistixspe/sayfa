<div class="advance-search clearfix">
	<div class="col-sm-4">
		<form class="form-inline" method="GET" action="<?php echo get_permalink(); ?>">

			<?php
			if (isset($_GET['page_id'])) {
			    ?><input type="hidden" name="page_id" value="<?php echo $_GET['page_id']; ?>" /><?php
			}
			?>

		  	<div class="form-group">
		    	<label for="posts_per_page">Products per page</label>
			    <select name="posts_per_page" id="posts_per_page" class="form-control" data-taxonomy="<?php echo $instance['product_category']; ?>" onchange="this.form.submit()">
					<!-- <option value="12" selected="">12</option> -->
					<option value="25">25</option>
					<option selected value="50">50</option>
					<option value="100">100</option>
				</select>
		  	</div>
		</form>
	</div>
	
	<div class="col-sm-8">
		<div class="form-group">
		<label for="find-product-category">Search in this category</label>
		    <input 
		    type="text" 
		    id="find-product-category" 
		    class="form-control" 
		    name="find-product-category"
		    data-taxonomy="<?php echo $instance['product_category']; ?>">
		</div>
	</div>
</div>		
		
<div class="clearfix">&nbsp;</div>

<div id="loop-products" class="row">

<?php 

if (@$_GET['posts_per_page']) {
	$posts_per_page = $_GET['posts_per_page'];	
} else {
	$posts_per_page = 50;
}


$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array (
	'is_paged' 	=> true,
	'post_type' => array( 'product' ),
	'product_category' => $instance['product_category'],
	'paged' 	=> $paged,
	'posts_per_page' => $posts_per_page,
	'orderby' => 'menu_order title',
	'order'   => 'ASC',
);

include(sprintf("%s/loop/products.php", dirname(__FILE__) ) );?>

</div>
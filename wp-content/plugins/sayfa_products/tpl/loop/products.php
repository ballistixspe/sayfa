<?php

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) :

	while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

		<div class="col-sm-3">

			<div class="entry">
				
				<?php echo '<a target="_blank" href="' . get_the_permalink() .'" title="' . the_title_attribute(array( 'before' => 'Permalink to: ', 'after' => '', 'echo' => false )) .'" rel="bookmark">'; ?>
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail'); } ?>
				<?php echo '</a>'; ?>
				

				<div class="description"><h5><?php the_title( ); ?></h5></div>

				<!-- <span class="link"> <i class="fa fa-plus"></i> </span> -->

				<?php echo '<a class="btn btn-primary btn-xs product_modal" href="' . get_the_permalink() .'">Preview</a>'; ?>
				

		</div></div>

	<?php endwhile; ?>

	<div class="clearfix">&nbsp;</div>

	<?php

	$big = 999999999; // need an unlikely integer

	//echo str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) );

	$paginate =  paginate_links( array(
		'base' => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $the_query->max_num_pages,
		'type' => 'array'
	) );

	if($paginate ) {
		echo '<div class="col-xs-12"><ul class="pagination">';
			foreach ( $paginate as $page ) {
				echo '<li>' . $page . '</li>';
			}
		echo '</ul></div>';
	}

	?>
	<?php wp_reset_postdata(  ); ?>

<?php else : ?>

	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>

<?php 
function product_modal_shortcode( $atts, $content = null ) {
	$a = shortcode_atts( array(
    	'href' => ''
	), $atts );
	return '<a class="product_modal" href="' . $a['href'] . '">' . $content . '</a>';
}
add_shortcode( 'product_modal', 'product_modal_shortcode' );
<?php
if (!class_exists('SAYFA_PRODUCTS_CONSTRUCT')) {

    /**
     * A PostTypeTemplate class
     */
    class SAYFA_PRODUCTS_CONSTRUCT
    {

        /**
         * The Constructor
         */
        public function __construct()
        {

            // register actions

            add_action('init', array(&$this, 'init'));
        }
        // END public function __construct()

        /**
         * hook into WP's init action hook
         */
        public function init()
        {

            // Initialize Post Type
            $this->create_post_type('product', 'dashicons-cart');
            $this->create_taxonomy('product_category', 'product');
            $this->create_taxonomy('brand', 'product');
            add_action("template_redirect", array(&$this, 'my_theme_redirect'));
            add_action('wp_enqueue_scripts', array(&$this, 'plugin_enqueue_styles'));
            add_action('wp_footer', array(&$this,'product_modal_box'), 100);
        }
        // END public function init()

        public function product_modal_box()
        {
            echo '<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3 class="modal-title" id="productModalLabel"></h3></div><div class="modal-body"></div></div></div></div>';
        }

        public function plugin_enqueue_styles()
        {
            wp_enqueue_script(
                'sayfa_products',
                plugins_url('/js/sayfa_products.js', dirname(__FILE__)),
                array('jquery'),
                '1.0.22',
                true
            );

            // wp_enqueue_style(
            //     'sayfa_products',
            //     plugins_url('/css/styles.css', dirname(__FILE__)),
            //     array(),
            //     '1.0.0'
            // );

            wp_localize_script(
                'sayfa_products',
                'sayfa_products',
                array(
                    'nonce' => wp_create_nonce('sayfa_products_nonce'),
                    'url'   => admin_url('admin-ajax.php'),
                )
            );
        }
        /**
         * Create the post type
         */
        public function create_taxonomy($taxonomy, $post_type)
        {
            register_taxonomy(
                $taxonomy,
                array($post_type),
                array(
                    'hierarchical'      => true,
                    'labels'            => array(
                        'name'              => __(sprintf('%s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'singular_name'     => __(ucwords(str_replace("_", " ", $taxonomy))),
                        'search_items'      => __(sprintf('Search %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'all_items'         => __(sprintf('All %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'parent_item'       => __(sprintf('Parent %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'parent_item_colon' => __(sprintf('Parent %s:', ucwords(str_replace("_", " ", $taxonomy)))),
                        'edit_item'         => __(sprintf('Edit %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'update_item'       => __(sprintf('Update %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'add_new_item'      => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'new_item_name'     => __(sprintf('New %s name', ucwords(str_replace("_", " ", $taxonomy)))),
                        'menu_name'         => __(sprintf('%s', ucwords(str_replace("_", " ", $taxonomy)))),
                    ),
                    'has_archive'       => true,
                    'show_ui'           => true,
                    'show_admin_column' => true,
                    'query_var'         => true,
                    'rewrite'           => array('
                        slug' => $post_type,
                    ),
                )
            );
        }
        /**
         * Create the post type
         */
        public function create_post_type($post_type, $menu_icon)
        {
            register_post_type($post_type, array(
                'hierarchical'        => true,
                'labels'              => array(
                    'name'               => __(sprintf('%ss', ucwords(str_replace("_", " ", $post_type)))),
                    'singular_name'      => __(ucwords(str_replace("_", " ", $post_type))),
                    'add_new'            => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $post_type)))),
                    'add_new_item'       => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $post_type)))),
                    'not_found'          => __('Nothing found'),
                    'not_found_in_trash' => __('Nothing found in Trash'),
                    'parent_item_colon'  => '',
                ),
                'public'              => true,
                'has_archive'         => true,
                'show_ui'             => true,
                'publicly_queryable'  => true,
                'exclude_from_search' => false,
                'menu_position'       => 5,
                'supports'            => array(
                    'title',
                    'editor',
                    'page-attributes',
                    'thumbnail',
                ),
                'rewrite'             => array(
                    'slug'       => $post_type,
                    'with_front' => false,
                ),
                'menu_icon'           => $menu_icon,
            ));
        }
        public function my_theme_redirect()
        {

            global $wp_query;

            if ($wp_query->query_vars['post_type'] == 'product') {

                if (is_archive()) {
                    $templatefilename = 'archive-product.php';
                } else {
                    $templatefilename = 'single-product.php';
                }

                if (file_exists(TEMPLATEPATH . '/' . $templatefilename)) {
                    $return_template = TEMPLATEPATH . '/' . $templatefilename;

                } else {
                    $return_template = sprintf("%1s/../tpl/%2s", dirname(__FILE__), $templatefilename);
                }

                $this->do_theme_redirect($return_template);

            }
        }

        public function do_theme_redirect($url)
        {
            global $post, $wp_query;
            if (have_posts()) {
                include $url;
                die();
            } else {
                $wp_query->is_404 = true;
            }
        }
    }
    // END class SAYFA_PRODUCTS_CONSTRUCT
}
// END if(!class_exists('SAYFA_PRODUCTS_CONSTRUCT'))

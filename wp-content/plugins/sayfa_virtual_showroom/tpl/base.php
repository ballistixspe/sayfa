<?php
$args = array (
		'post_type'		=> array( 'building' ),
		'p' 			=> $instance['building_id']
);


$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) : 
	echo '<div id="virtual-showroom">';
	while ( $the_query->have_posts() ) : $the_query->the_post(); 
	
		echo '<div class="hotspots-area">';

		$image = get_field('building_image');
		
		if( !empty($image) ): 
			echo '<img class="img-responsive" src="' . $image['url'] . '" alt="' . $image['alt'] . '" />';
		endif; 
		
		if( have_rows('hotspots') ):

			$count = 1;
		
			echo '<ul class="hotspots-area-links">';

			while ( have_rows('hotspots') ) : the_row(); ?>
				<li style="top: <?php the_sub_field('top'); ?>%; left: <?php the_sub_field('left'); ?>%;">

					<a id="hotspotModalToggle"
					href="<?php echo get_the_permalink(get_sub_field('post')) ?>"
					title="<?php echo the_title_attribute(array( 'before' => 'Permalink to: ', 'after' => '', 'echo' => false ))?>" 
					rel="bookmark">
					<?php echo $count; ?></a></li>
			<?php
			$count++;
			endwhile;
			echo '</ul>';
		
		endif; 

		echo '</div>';
		echo '<div class="hotspots-description"><h3 class="text-uppercase">' . get_the_title() .'</h3>' . get_field('description') . '</div>';

	endwhile; wp_reset_postdata(); 

	echo '</div>';

?>

<!-- Modal -->
<!-- <div class="modal fade" id="hotspotModal" tabindex="-1" role="dialog" aria-labelledby="hotspotModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="hotspotModalLabel"></h3>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div> -->

<?php endif; ?>
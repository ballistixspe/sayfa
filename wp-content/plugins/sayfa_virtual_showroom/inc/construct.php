<?php
if (!class_exists('SAYFA_VIRTUAL_SHOWROOM_CONSTRUCT')) {

    /**
     * A PostTypeTemplate class
     */
    class SAYFA_VIRTUAL_SHOWROOM_CONSTRUCT
    {

        /**
         * The Constructor
         */
        public function __construct()
        {

            // register actions

            add_action('init', array(&$this, 'init'));

        }
        // END public function __construct()

        /**
         * hook into WP's init action hook
         */
        public function init()
        {

            // Initialize Post Type
            $this->create_post_type('building', 'dashicons-admin-home');

        }
        // END public function init()

        /**
         * Create the post type
         */
        public function create_taxonomy($taxonomy, $post_type)
        {

            register_taxonomy(
                $taxonomy,
                array($post_type),
                array(
                    'hierarchical'      => true,
                    'labels'            => array(
                        'name'              => __(sprintf('%s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'singular_name'     => __(ucwords(str_replace("_", " ", $taxonomy))),
                        'search_items'      => __(sprintf('Search %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'all_items'         => __(sprintf('All %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'parent_item'       => __(sprintf('Parent %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'parent_item_colon' => __(sprintf('Parent %s:', ucwords(str_replace("_", " ", $taxonomy)))),
                        'edit_item'         => __(sprintf('Edit %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'update_item'       => __(sprintf('Update %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'add_new_item'      => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $taxonomy)))),
                        'new_item_name'     => __(sprintf('New %s name', ucwords(str_replace("_", " ", $taxonomy)))),
                        'menu_name'         => __(sprintf('%s', ucwords(str_replace("_", " ", $taxonomy)))),
                    ),
                    'has_archive'       => true,
                    'show_ui'           => true,
                    'show_admin_column' => true,
                    'query_var'         => true,
                    'rewrite'           => array('
                        slug' => $post_type,
                    ),
                ));

        }
        /**
         * Create the post type
         */
        public function create_post_type($post_type, $menu_icon)
        {
            register_post_type($post_type, array(
                'hierarchical'        => true,
                'labels'              => array(
                    'name'               => __(sprintf('%ss', ucwords(str_replace("_", " ", $post_type)))),
                    'singular_name'      => __(ucwords(str_replace("_", " ", $post_type))),
                    'add_new'            => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $post_type)))),
                    'add_new_item'       => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $post_type)))),
                    'not_found'          => __('Nothing found'),
                    'not_found_in_trash' => __('Nothing found in Trash'),
                    'parent_item_colon'  => '',
                ),
                'public'              => true,
                'has_archive'         => true,
                'show_ui'             => true,
                'publicly_queryable'  => true,
                'exclude_from_search' => false,
                'menu_position'       => 5,
                'supports'            => array(
                    'title',
                    'editor',
                    'page-attributes',
                    'thumbnail',
                ),
                'rewrite'             => array(
                    'slug'       => $post_type,
                    'with_front' => false,
                ),
                'menu_icon'           => $menu_icon,
            ));
        }
    }
    // END class SAYFA_VIRTUAL_SHOWROOM_CONSTRUCT
}
// END if(!class_exists('SAYFA_VIRTUAL_SHOWROOM_CONSTRUCT'))

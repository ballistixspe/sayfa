<?php 


add_action("wp_ajax_sayfa_virtual_showroom_toggle", "sayfa_virtual_showroom_toggle");
add_action("wp_ajax_nopriv_sayfa_virtual_showroom_toggle", "sayfa_virtual_showroom_toggle");

function sayfa_virtual_showroom_toggle() {

	check_ajax_referer( 'sayfa_virtual_showroom_nonce', 'nonce' );
	$value = $_POST['value'];

	ob_start();

	$args = array (
		'post_type' => 'product',
		'p' => $value
	);
	$the_query = new WP_Query($args);
	if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 

	?>

	<div class="row">
			<div class="col-sm-4">
				
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>

				<a href="<?php echo $image[0]; ?>" id="image-placeholder" class="fancybox">
					<?php the_post_thumbnail('medium'); ?> 					
				</a>
				
				<div class="clearfix">&nbsp;</div>
			
			<table>
				<tr>
					<td><img src=" <?php echo plugins_url('/img/sayfa_logo_dark_small.png', dirname(__FILE__)); ?>" alt=""></td>
					<td>
						<small>
							<?php printf('<a href="tel:%s"><span>%s</span></a>', get_option('dsi_phone'),get_option('dsi_phone')); ?> <br>
							<?php printf('<a href="mailto:%s"><span>%s</span></a>', get_option('dsi_email'),get_option('dsi_email')); ?>
						</small>		
					</td>
				</tr>
			</table>

			</div>
			<div class="col-sm-8">
				
							<div role="tabpanel">
						    
							    <ul class="nav nav-tabs" role="tablist">

									<?php if (get_field('description', $post->ID )): ?>
							        <li role="presentation" class="active">
							            <a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a>
							        </li>
								    <?php endif; ?>
									
									<?php if (get_field('specification', $post->ID )): ?>
							        <li role="presentation">
							            <a href="#specification" aria-controls="specification" role="tab" data-toggle="tab">Specification</a>
							        </li>
								    <?php endif; ?>

							    	<?php if (get_field('pdf', $post->ID )): ?>
						            <li role="presentation">
						                <a href="#literature" aria-controls="literature" role="tab" data-toggle="tab">Literature</a>
						            </li>
							        <?php endif; ?>

									<?php if (get_field('gallery', $post->ID )): ?>
							        <li role="presentation">
							            <a href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a>
							        </li>
								    <?php endif; ?>

				        		    <?php if( have_rows('tech_drawings') ):
				    					echo '<li role="presentation">
							            <a href="#tech-drawings" aria-controls="tech-drawings" role="tab" data-toggle="tab">Tech Drawings</a></li>';
				    				endif; ?>

							    </ul>
							
								<div class="clearfix">&nbsp;</div>


							<!-- Tab panes -->

						    	<div class="tab-content">
						    	<?php if (get_field('description', $post->ID )): ?>
							        <div role="tabpanel" class="tab-pane active" id="description">
							             <?php echo wpautop(the_field('description', $post->ID )); ?>
							             
							        </div>
						    	<?php endif; ?>
								
								<?php if (get_field('specification', $post->ID )): ?>
							        <div role="tabpanel" class="tab-pane" id="specification">
							             <?php echo wpautop(the_field('specification', $post->ID )); ?>
							        </div>
						    	<?php endif; ?>

				    		    <?php if( have_rows('pdf') ):
									echo '<div role="tabpanel" class="tab-pane" id="literature">';
								    echo '<ul>';
								    while ( have_rows('pdf') ) : the_row();
								        $file = get_sub_field('file');
								        if( $file ): 
								            echo '<li><a href="' . $file['url'] . '" target="_blank">' . $file['title'] . '</li></a>';
								        endif;
								    endwhile;
								    echo '</ul>';
								    echo '</div>';
								endif; ?>
				    		        
								<?php 

								$images = get_field('gallery', $post->ID);

							  	if( $images ): ?>
								  	<div role="tabpanel" class="tab-pane" id="gallery">
								  	    <ul>
								  	        <?php foreach( $images as $image ): ?>
								  	            <li>
								  	                <a class="gallery-thumbnail" href="<?php echo $image['url']; ?>" data-full="<?php echo $image['url']; ?>">
								  	                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
								  	                </a>
								  	            </li>
								  	        <?php endforeach; ?>
								  	    </ul>
							  	    </div>
							  	<?php endif; ?>

				    		    <?php if( have_rows('tech_drawings') ):
									echo '<div role="tabpanel" class="tab-pane" id="tech-drawings">';
								    echo '<ul>';
								    while ( have_rows('tech_drawings') ) : the_row();
								        $file = get_sub_field('file');
								        if( $file ): 
								            echo '<li><a href="' . $file['url'] . '" target="_blank">' . $file['title'] . '</li></a>';
								        endif;
								    endwhile;
								    echo '</ul>';
								    echo '</div>';
								endif; ?>

								</div>
							</div>
							
			</div>
	</div>
	
	<?php
	endwhile; endif;
	$data = ob_get_clean();
	wp_send_json_success( $data );
	wp_die();
};
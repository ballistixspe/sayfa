<?php

/*
Plugin Name: Sayfa: Virtual Showroom
Plugin URI: http://ballistix.com/
Description: SAYFA_VIRTUAL_SHOWROOM
Version: 1.0
Author: Ballistix SPE
Author URI: http://ballistix.com/
License: GPL2
*/

/*
Copyright 2015  Marcel Badua  (email : marcel.badua@ballistix.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if (!class_exists('SAYFA_VIRTUAL_SHOWROOM')) {
    class SAYFA_VIRTUAL_SHOWROOM
    {
        /**
         * Construct the plugin object
         */
        public function __construct() {

            // require_once (sprintf("%s/so/so-widget.php", dirname(__FILE__)));

            require_once( sprintf("%s/inc/actions.php", dirname(__FILE__)) );
            
            require_once( sprintf("%s/inc/widget.php", dirname(__FILE__)) );
            $SAYFA_VIRTUAL_SHOWROOM_WIDGET = new SAYFA_VIRTUAL_SHOWROOM_WIDGET();

            require_once (sprintf("%s/inc/construct.php", dirname(__FILE__)));
            $SAYFA_VIRTUAL_SHOWROOM_CONSTRUCT = new SAYFA_VIRTUAL_SHOWROOM_CONSTRUCT();


        } // END public function __construct

        /**
         * Activate the plugin
         */
        public static function activate() {
            // Do nothing
        } // END public static function activate
        
        /**
         * Deactivate the plugin
         */
        public static function deactivate() {
            // Do nothing
        }// END public static function deactivate

    } // END class SAYFA_VIRTUAL_SHOWROOM
} // END if(!class_exists('SAYFA_VIRTUAL_SHOWROOM'))

if (class_exists('SAYFA_VIRTUAL_SHOWROOM')) {
    // Installation and uninstallation hooks
    register_activation_hook(__FILE__, array('SAYFA_VIRTUAL_SHOWROOM','activate'));
    register_deactivation_hook(__FILE__, array( 'SAYFA_VIRTUAL_SHOWROOM', 'deactivate'));
    // instantiate the plugin class
    $wp_plugin_template = new SAYFA_VIRTUAL_SHOWROOM();
}

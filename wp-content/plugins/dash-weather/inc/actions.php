<?php

add_action('init', 'location_cookie');

function location_cookie()
{
    if (!isset($_COOKIE['location'])) {
        $location_cookie = 0;
        setcookie('location', $location_cookie, time() + (86400 * 30), "/");
    }
}

add_action("wp_ajax_set_location_cookie", "set_location_cookie");
add_action("wp_ajax_nopriv_set_location_cookie", "set_location_cookie");

function set_location_cookie()
{
    check_ajax_referer( 'dash_weather_nonce', 'nonce' );
	$location_cookie = $_POST['location'];
    setcookie('location', $location_cookie,  time() + (86400 * 30), "/");
    ob_start();
    $data = ob_get_clean();
    wp_send_json_success($data);
    wp_die();

}

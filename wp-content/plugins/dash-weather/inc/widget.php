<?php

/**
 * Register the widget
 */
function REGISTER_DASH_WEATHER_WIDGET() {
    register_widget( 'DASH_WEATHER_WIDGET' );
}
add_action( 'widgets_init', 'REGISTER_DASH_WEATHER_WIDGET' );

/**
 * Class DASH_WEATHER_WIDGET
 */
class DASH_WEATHER_WIDGET extends WP_Widget
{
    /** Basic Widget Settings */
    const WIDGET_NAME        = "Dash Weather Widget";
    const WIDGET_DESCRIPTION = "This widget for Dash Weather";
    public $textdomain;
    public $fields;
    /**
     * Construct the widget
     */
    public function __construct()
    {
        //We're going to use $this->textdomain as both the translation domain and the widget class name and ID
        $this->textdomain = strtolower(get_class($this));

        //Figure out your textdomain for translations via this handy debug print
        //var_dump($this->textdomain);

        //Add fields
        $this->add_field('title', 'Enter title', '', 'text');
        //$this->add_field('product_category', 'Product Category (slug)', '', 'text');

        //Translations
        //load_plugin_textdomain($this->textdomain, false, basename(dirname(__FILE__)) . '/languages' );

        //Init the widget
        parent::__construct($this->textdomain, __(self::WIDGET_NAME, $this->textdomain), array('description' => __(self::WIDGET_DESCRIPTION, $this->textdomain), 'classname' => $this->textdomain));
    }
    /**
     * Widget frontend
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {

        $this->plugin_enqueue();

        add_action( 'wp_footer', array(&$this,'dash_weather_widget_modal'), 100 );

        $title = @apply_filters('widget_title', $instance['title']);
        /* Before and after widget arguments are usually modified by themes */
        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        /* Widget output here */
        $this->widget_output($args, $instance);
        /* After widget */
        echo $args['after_widget'];
    }

    public function dash_weather_widget_modal() {
            $location = '';?>
            <div class="modal fade" id="modal-id">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>

                        <div class="modal-body">
                            <div class="col-sm-4">
                              <div id="weather" class="well text-center">

                              </div>
                            </div>
                            <div class="col-sm-8">
                                <table id="forecast" class="table table-striped"></table>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>
                        <div class="modal-footer">
                            <label for="">Change your city: </label> <input type="text" class="select-city">
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }

    /**
     * This function will execute the widget frontend logic.
     * Everything you want in the widget should be output here.
     */
    private function widget_output($args, $instance)
    {
        extract($instance);
        /**
         * This is where you write your custom code.
         */

         if (!isset($_COOKIE['location'])) {
             printf('<a id="weather-toggle" data-toggle="modal" href="#modal-id" data-location="0">Weather Loading</a>');
         } else {
         	printf('<a id="weather-toggle" data-toggle="modal" href="#modal-id" data-location="%s">Weather Loading</a>', $_COOKIE['location']);
         }
    }

    public function plugin_enqueue()
    {

        $args = array(
          'nonce' => wp_create_nonce( 'dash_weather_nonce' ),
          'url'   => admin_url( 'admin-ajax.php' )
          );

        wp_enqueue_script(
            'dash_weather',
            plugins_url('/js/dash-weather.js', dirname(__FILE__)),
            array('jquery'),
            '1.1.0',
            true
        );

        wp_localize_script(
            'dash_weather',
            'dash_weather',
            $args
        );

    }
    /**
     * Widget backend
     *
     * @param array $instance
     * @return string|void
     */
    public function form($instance)
    {

        echo '<p>' . self::WIDGET_DESCRIPTION . '</p>';

        /* Generate admin for fields */
        foreach ($this->fields as $field_name => $field_data) {
            if ($field_data['type'] === 'text'):
            ?>
        <p>
          <label for="<?php echo $this->get_field_id($field_name); ?>"><?php _e($field_data['description'], $this->textdomain);?></label>
          <input class="widefat" id="<?php echo $this->get_field_id($field_name); ?>" name="<?php echo $this->get_field_name($field_name); ?>" type="text" value="<?php echo esc_attr(isset($instance[$field_name]) ? $instance[$field_name] : $field_data['default_value']); ?>" />
        </p>
      <?php
//elseif($field_data['type'] == 'textarea'):
            //You can implement more field types like this.
            else:
                echo __('Error - Field type not supported', $this->textdomain) . ': ' . $field_data['type'];
            endif;
        }
    }
    /**
     * Adds a text field to the widget
     *
     * @param $field_name
     * @param string $field_description
     * @param string $field_default_value
     * @param string $field_type
     */
    private function add_field($field_name, $field_description = '', $field_default_value = '', $field_type = 'text')
    {
        if (!is_array($this->fields)) {
            $this->fields = array();
        }

        $this->fields[$field_name] = array(
            'name'          => $field_name,
            'description'   => $field_description,
            'default_value' => $field_default_value,
            'type'          => $field_type,
        );
    }
    /**
     * Updating widget by replacing the old instance with new
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        return $new_instance;
    }
}



                            // $city_arr = json_decode( file_get_contents( plugins_url( ) . '/dash-weather/js/city.json' ) , true);
                            // asort($city_arr);
                            // echo 'Change City <select class="select-city">';
                            // foreach ($city_arr as $value) {
                            //     $city = $value['city'] .', ' . $value['country'];
                            //     if ( $location == $city ) {
                            //         echo '<option value="'. $city.'" selected>' . $city . '</option>';
                            //     } else {
                            //         echo '<option value="'. $city.'">' . $city . '</option>';
                            //     }
                            // }
                            // echo '</select>'

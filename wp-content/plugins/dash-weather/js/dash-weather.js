jQuery.noConflict();
(function($) {
  location_cookie = $('#weather-toggle').data('location');
  if (location_cookie === 0) {
    $.get("https://ipinfo.io", function(response) {
      var location = response.city + ", " + response.country;
      getOpenWeatherMap(location);
    }, "jsonp");
  } else {
    getOpenWeatherMap(location_cookie);
  }

  function getOpenWeatherMap(location) {
    $.getJSON("https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=2e4dcd4eacaded7a903079984fefa3af&units=metric").done(function(data) {
      const mydate = new Date(data.dt * 1000);
      var date = mydate.getDate();
      var month = mydate.getMonth();
      var year = mydate.getFullYear();
      var dateWithFullMonthName = monthNames[month] + " " + pad(date) + ", " + year;

      $('#weather').append('<p">' + dateWithFullMonthName + '</p>');
      $("#weather").append('<img src="https://openweathermap.org/img/w/' + data.weather[0].icon + '.png">');
      $("#weather").append('<p class="h3">' + data.main.temp + '&#8451;</p>');
      $("#weather").append('<p>' + data.weather[0].description + '</p>');

      $("#weather").append('<p><b>Wind</b><br/>' + data.wind.speed + '</p>');
      $("#weather").append('<p><b>Pressure</b><br/>' + data.main.pressure + '</p>');
      $("#weather").append('<p><b>Humidity</b><br/>' + data.main.humidity + '</p>');
      $("#weather").append('<p><b>Clouds</b><br/>' + data.clouds.all + '</p>');

      $('#weather-toggle').html('<span class="temp">' + data.main.temp + '&#8451;</span> <span class="city">' + data.name + ' ' + data.sys.country + '</span>');

    });

    $.getJSON("https://api.openweathermap.org/data/2.5/forecast?&q=" + location + "&appid=2e4dcd4eacaded7a903079984fefa3af&units=metric").done(function(data) {
      var forecast;
      $("#forecast").html();
      $('#modal-id .modal-title').html(data.city.name + ' ' + data.city.country);
      var i;
      for (i = 0; i < data.list.length; i++) {
        if ((i % 8) == 0) {
          const mydate = new Date(data.list[i].dt * 1000);
          var date = mydate.getDate();
          var month = mydate.getMonth();
          var year = mydate.getFullYear();
          var dateWithFullMonthName = monthNames[month] + " " + pad(date) + ", " + year;
          $("#forecast").append('<tr><td>' + dateWithFullMonthName + '</td><td>' + data.list[i].weather[0].description + '</td><td class="no-wrap">' + data.list[i].main.temp_min + '&#8451;</td><td class="no-wrap">' + data.list[i].main.temp_max + '&#8451;</td></tr>');
        }
      }
    });
  }

  function pad(n) {
    return n < 10 ? '0' + n : n;
  }
  var monthNames = [
    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
  ];
  $(document).on('change', '.select-city', function() {
    var data = {
      action: 'set_location_cookie',
      nonce: dash_weather.nonce,
      location: $(this).val()
    };
    $.ajax({
      type: 'post',
      url: dash_weather.url,
      cache: false,
      data: data,
      beforeSend: function() {},
      success: function(response) {
        getOpenWeatherMap(data.location);
      }
    });
    return false;
  })
})(jQuery);

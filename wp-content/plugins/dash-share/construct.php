<?php



if (!class_exists('DASH_SHARE_CONSTRUCT')) {

    /**
     * A PostTypeTemplate class
     */
    class DASH_SHARE_CONSTRUCT
    {

        /**
         * The Constructor
         */
        public function __construct()
        {

            // register actions
            add_action('init', array(&$this, 'init'));
            
        }
        // END public function __construct()

        /**
         * hook into WP's init action hook
         */
        public function init()
        {
            require_once 'Mobile_Detect.php';
            $detect = new Mobile_Detect;
            
            if ( !$detect->isMobile() ) {
                add_action('wp_enqueue_scripts', array(&$this, 'plugin_enqueue'));
                add_action( 'wp_footer', array(&$this,'append_to_footer'), 100 );

            }
            
        }
        // END public function init()

        public function plugin_enqueue()
        {
            
            wp_enqueue_style(
                'dash_share', 
                plugins_url('/css/dash-share.min.css', dirname(__FILE__)),
                false,
                'all' 
            );

            wp_enqueue_script(
                'dash_share', 
                plugins_url('/js/dash-share.min.js', dirname(__FILE__)),
                array('jquery'), 
                '1.0.0', 
                true
            );

        }

        public function  append_to_footer() { 
            ?>
            <script>
                (function($) {
                    $("body").defaultPluginName({
                        propertyName: "<?php echo get_option('share_message'); ?>"
                    });
                })(jQuery);
            </script>
            <?php
        } 
    }
    // END class DASH_SHARE_CONSTRUCT
}
// END if(!class_exists('DASH_SHARE_CONSTRUCT'))

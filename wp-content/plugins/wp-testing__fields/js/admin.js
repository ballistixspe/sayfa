(function(angular, initFields, initFieldTypes) {
	'use strict';

	angular.module('wptApp')
		.value('fields', initFields)
		.value('fieldTypes', initFieldTypes)
		.controller('EditFieldsController', EditFieldsController);

	EditFieldsController.$inject = ['$scope', 'fields', 'fieldTypes', 'locale'];
	function EditFieldsController  ( $scope,   fields,   fieldTypes,   locale) {
		$scope.fields = fields;
		$scope.types  = fieldTypes;
		$scope.__     = locale;

		$scope.addField = function addField(type) {
			$scope.fields.push({
				id             : null,
				type           : type.key,
				title          : type.defaultFieldTitle,
				is_required    : false,
				clarification  : null,
				type_full_label: type.fullLabel,
				type_allow_list: type.allowList
			});
		};

		$scope.removeField = function removeField($index, field) {
			if (field.has_values && !confirm(locale.hasValuesAreYouSure)) {
				return;
			}
			$scope.fields.splice($index, 1 );
		};
	};
})(angular, Wpt.fields, Wpt.fieldTypes);
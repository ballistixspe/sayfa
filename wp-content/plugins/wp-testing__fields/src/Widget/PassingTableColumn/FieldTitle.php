<?php

class WpTestingFields_Widget_PassingTableColumn_FieldTitle extends WpTesting_Widget_ListTableColumn
{
	private $key;
	private $title;

	public function __construct(WpTestingFields_Model_Field $field)
	{
		$this->key   = $field->getSlug();
		$this->title = $field->getTitle();
	}

	public function key()
	{
		return 'field-' . $this->key;
	}

	public function placeAfter()
	{
		return 'results';
	}

	public function title()
	{
		return $this->title;
	}

	public function value($item)
	{
		$passing = new WpTestingFields_Model_Passing($item);
		$values  = $passing->getFieldValuesBySlug($this->key);
		return (empty($values)) ? '-' : implode(', ', $values);
	}
}

<?php

class WpTestingFields_Widget_PassingTableColumn_FieldValue extends WpTesting_Widget_ListTableColumn
{
	/**
	 * @var WpTestingFields_Model_Field
	 */
	private $field;

	public function __construct(WpTestingFields_Model_Field $field)
	{
		$this->field = $field;
	}

	public function key()
	{
		return 'field-' . $this->field->getId();
	}

	public function placeAfter()
	{
		return 'results';
	}

	public function title()
	{
		return $this->field->getTitle();
	}

	public function value($item)
	{
		$passing = new WpTestingFields_Model_Passing($item);
		$values  = $passing->buildValues();

		foreach ($values as $value) {
			if ($value->getFieldId() == $this->field->getId()) {
				return $value->getValue();
			}
		}

		return '-';
	}
}

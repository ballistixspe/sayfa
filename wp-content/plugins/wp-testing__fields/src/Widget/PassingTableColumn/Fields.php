<?php

class WpTestingFields_Widget_PassingTableColumn_Fields extends WpTesting_Widget_ListTableColumn
{

	public function key()
	{
		return 'fields';
	}

	public function placeAfter()
	{
		return 'results';
	}

	public function title()
	{
		return __('Fields', 'wp-testing-fields');
	}

	public function value($item)
	{
		$passing = new WpTestingFields_Model_Passing($item);
		$values  = $passing->buildValues();

		$result  = array();
		foreach ($values as $value) {
			$result[] = sprintf('<div><span class="title">%s</span><span class="value">%s</span></div>', $value->createField()->getTitle(), $value->getValue());
		}

		return implode('', $result);
	}
}
<?php

class WpTestingFields_Doer_PassingBrowser extends WpTesting_Doer_AbstractDoer
{

	public function renderAdminPassingsPage(WpTesting_Widget_PassingTable $table)
	{
		$passings = $table->prepare_items()->items;
		$fields   = WpTestingFields_Query_Field::create()->findAllByPassings($passings);
		foreach ($fields as $field) {
			$table->add_dynamic_column(new WpTestingFields_Widget_PassingTableColumn_FieldTitle($field));
		}

		$this->enqueueStyle('admin');
	}
}

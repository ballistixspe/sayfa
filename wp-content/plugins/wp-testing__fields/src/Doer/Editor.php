<?php

class WpTestingFields_Doer_Editor extends WpTesting_Doer_AbstractDoer
{
	protected function getClassFile()
	{
		return __FILE__;
	}

	public function customizeUi()
	{
		$this->wp
			->addMetaBox(
				'wpt__fields_edit_fields',
				__('Edit Form Elements', 'wp-testing-fields'),
				array($this, 'renderMetabox'),
				'wpt_test'
			)
			->addFilter('wpt_test_editor_test_page_options', array($this, 'addTestPageOptions'))
		;

		$test = $this->createTest($this->getRequestValue('post'));
		$this
			->enqueueScript('admin', array('angular'))
			->enqueueStyle('admin')
			->addJsData('fields', $this->toJson($test->buildFields()))
			->addJsData('fieldTypes', $this->toJson($this->getFieldTypes()))
			->addJsData('locale', array(
				'type'              => __('Type', 'wp-testing-fields'),
				'formElement'       => __('Form Element', 'wp-testing-fields'),
				'label'             => __('Label', 'wp-testing-fields'),
				'isRequired'        => __('Is required?', 'wp-testing-fields'),
				'clarificationText' => __('Clarification Text', 'wp-testing-fields'),
				'noFormElements'	=> __('No form elements to edit. Click on buttons below to add them.', 'wp-testing-fields'),
				'fields'            => __('Fields', 'wp-testing-fields'),
				'listValues'        => __('List Values', 'wp-testing-fields'),
				'hasValuesAreYouSure' => __('This field has values in "Respondents’ results" that will be lost also. Are you sure you want to delete it?', 'wp-testing-fields'),
			))
		;
	}

	/**
	 * @param WP_Post $item
	 */
	public function renderMetabox($item)
	{
		$this->output('Editor/metabox');
	}

	public function addTestPageOptions($options)
	{
		return $options + array(
			'wpt_test_page_denote_required_fields' => array(
				'break'   => true,
				'default' => '0',
				'title'   => __('Denote required fields', 'wp-testing-fields'),
			),
			'wpt_test_page_denote_optional_fields' => array(
				'default' => '1',
				'title'   => __('Denote optional fields', 'wp-testing-fields'),
			),
			'wpt_test_page_place_fields' => array(
				'default' => 'before_questions',
				'title'   => __('Place fields', 'wp-testing-fields'),
				'type'    => 'select',
				'values'  => array(
					'before_questions' => __('before questions', 'wp-testing-fields'),
					'after_questions'  => __('after questions', 'wp-testing-fields'),
				),
			),
		);
	}

	/**
	 * Creates test with injected WP facade
	 * @param mixed $key
	 * @return WpTestingFields_Model_Test
	 */
	protected function createTest($key = null)
	{
		$test = new WpTestingFields_Model_Test($key);
		return $test->setWp($this->wp);
	}

	protected function getFieldTypes()
	{
		return array(
			new WpTestingFields_Model_FieldType_Text(),
			new WpTestingFields_Model_FieldType_Number(),
			new WpTestingFields_Model_FieldType_Email(),
			new WpTestingFields_Model_FieldType_Dropdown(),
			new WpTestingFields_Model_FieldType_Checkbox(),
			new WpTestingFields_Model_FieldType_Url(),
			new WpTestingFields_Model_FieldType_Date(),
		);
	}
}

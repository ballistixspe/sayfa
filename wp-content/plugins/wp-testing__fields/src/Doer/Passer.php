<?php

class WpTestingFields_Doer_Passer extends WpTesting_Doer_AbstractDoer
{

	/**
	 * @var WpTesting_Model_Passing
	 */
	private $passing;

	/**
	 * @var WpTestingFields_Model_Test
	 */
	private $test;

	protected function getClassFile()
	{
		return __FILE__;
	}

	public function setupFillForm(WpTesting_Model_Passing $passing, WpTestingFields_Model_Test $test)
	{
		$this->wp
			->addAction('wp_testing_passer_fill_form_after_render',          array($this, 'clearHandlers'))
			->addAction('wp_testing_template_fill_form_questions_before',    array($this, 'renderFormMessage'))
			->addAction('wp_testing_template_fill_form_label_end',           array($this, 'renderLabelEnd'))
			->addFilter('wp_testing_passer_fill_form_form_classes',          array($this, 'addFormClasses'))
		;

		$this->passing = $passing;
		$this->test    = $test;

		$step = $passing->getCurrentStep();
		if ($step->isFirst() && $test->isPlaceFieldsBeforeQuestions()) {
			$this->wp->addAction('wp_testing_template_fill_form_questions_before',   array($this, 'renderFieldsBeforeQuestions'));
		} elseif ($step->isLast() && $test->isPlaceFieldsAfterQuestions()) {
			$this->wp->addAction('wp_testing_template_fill_form_questions_after',    array($this, 'renderFieldsAfterQuestions'));
		}

		$this->enqueueStyle('style');
	}

	public function clearHandlers()
	{
		$this->wp
			->removeAction('wp_testing_passer_fill_form_after_render',       array($this, 'clearHandlers'))
			->removeAction('wp_testing_template_fill_form_questions_before', array($this, 'renderFormMessage'))
			->removeAction('wp_testing_template_fill_form_label_end',        array($this, 'renderLabelEnd'))
			->removeFilter('wp_testing_passer_fill_form_form_classes',       array($this, 'addFormClasses'))
			->removeAction('wp_testing_template_fill_form_questions_before', array($this, 'renderFieldsBeforeQuestions'))
			->removeAction('wp_testing_template_fill_form_questions_after',  array($this, 'renderFieldsAfterQuestions'))
		;
	}

	public function renderFormMessage()
	{
		$this->output('Passer/form-message');
	}

	/**
	 * @param array $options [required => boolean]
	 */
	public function renderLabelEnd($options = array())
	{
		$this->output('Passer/field-label-end', $options + array(
			'required' => null,
		));
	}

	public function renderFieldsBeforeQuestions()
	{
		$this->renderFields(array(
			'autofocus' => true,
		));
	}

	public function renderFieldsAfterQuestions()
	{
		$this->renderFields(array(
			'autofocus' => false,
		));
	}

	private function renderFields($renderParams = array())
	{
		$fieldValuePrefix = fORMRelated::determineRequestFilter('WpTesting_Model_Passing', 'WpTestingFields_Model_FieldValue', 'passing_id');
		$this->output('Passer/form-fields', $renderParams + array(
			'fields'               => $this->test->buildFields(),
			'fieldValueOrmPrefix'  => $fieldValuePrefix,
			'wp'                   => $this->wp,
		));
	}

	/**
	 * @param array $formClasses
	 * @return array
	 */
	public function addFormClasses($formClasses)
	{
		if (!$this->test->isDenoteRequiredFields()) {
			$formClasses[] = 'hide-requireds';
		}
		if (!$this->test->isDenoteOptionalFields()) {
			$formClasses[] = 'hide-optionals';
		}
		return $formClasses;
	}
}

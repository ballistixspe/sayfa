<div ng-controller="EditFieldsController" class="wptApp" ng-cloak>

<input type="hidden" name="wpt__fields_json" value="{{fields | json}}" />

<table class="widefat wpt__fields">
	<tr>
		<th class="bar"><?php echo __('#', 'wp-testing') ?></th>
		<th class="bar">{{ __.type }}</th>
		<th class="bar" colspan="2">{{ __.formElement }}</th>
	</tr>
	<tr ng-repeat="field in fields" class="wpt__field wpt__field_type_{{field.type}}" ng-class-even="'alternate'">
		<th class="wpt_number bar">
			{{$index + 1}}
		</th>
		<td class="wpt_type">
			{{field.type_full_label}}
		</td>
		<td class="wpt_details">
			<div class="wpt_value wpt_value_text wpt_value_title">
				<label>{{ __.label }}</label>
				<input type="text" class="wpt_text" ng-model="field.title" required />
			</div>
			<div class="wpt_value wpt_value_checkbox">
				<input type="checkbox" ng-model="field.is_required" id="wpt__fields_is_required_{{$index}}" />
				<label for="wpt__fields_is_required_{{$index}}">{{ __.isRequired }}</label>
			</div>
			<div class="wpt_value wpt_value_text wpt_value_clarification">
				<label>{{ __.clarificationText }}</label>
				<input type="text" class="wpt_text" ng-model="field.clarification" />
			</div>
			<div ng-if="field.type_allow_list" class="wpt_value_textarea wpt_value_list">
				<label>{{ __.listValues }}</label>
				<textarea ng-model="field.list_values"></textarea>
			</div>
		</td>
		<td class="actions">
			<button ng-click="removeField($index, field)" type="button" class="notice-dismiss"></button>
		</td>
	</tr>
	<tr ng-if="fields.length == 0" class="alternate">
		<td colspan="4">
			<p class="highlight">
				{{ __.noFormElements }}
			</p>
		</td>
	</tr>
</table>

<table class="widefat wpt__add_field">
	<tr>
		<th class="bar">{{ __.fields }}</th>
	</tr>
	<tr>
		<td><span ng-repeat="type in types">
			<button ng-click="addField(type)" type="button" class="button">{{ type.label }}</button>
		</span></td>
	</tr>
</table>

</div>

<?php
/* @var $autofocus boolean */
/* @var $fields WpTestingFields_Model_Field[] */
/* @var $fieldValueOrmPrefix string */
/* @var $wp WpTesting_WordPressFacade */
?>
<div class="wpt__fields">
<?php foreach ($fields as $f => $field): ?>
<?php $type = $field->createType() ?>
<?php $isCheckBox               = ($type instanceof WpTestingFields_Model_FieldType_Checkbox) ?>
<?php $isRenderInputInsideLabel = $isCheckBox ?>
<?php $isRenderInputAfterLabel  = !$isRenderInputInsideLabel ?>

<?php ob_start() ?>
<?php if(false): ?><input<?php endif ?>
		<?php if (0 == $f && $autofocus): ?>autofocus="autofocus"<?php endif ?>
		<?php if($field->getIsRequired()): ?>required="required" aria-required="true"<?php endif ?>
		id="wpt__fields_<?php echo $f ?>"
		name="<?php echo $fieldValueOrmPrefix ?>field_value[<?php echo $f ?>]"
<?php if(false): ?>/><?php endif ?>
<?php $attributesHTML = ob_get_clean() ?>

<?php ob_start() ?>
<?php if ($isCheckBox): ?>
	<input type="hidden" name="<?php echo $fieldValueOrmPrefix ?>field_value[<?php echo $f ?>]" value="0" />
<?php endif ?>
<?php if ($type instanceof WpTestingFields_Model_FieldType_Dropdown): ?>
	<select
		<?php echo $attributesHTML ?>
		class="wpt__fields_select">
		<option value="" disabled="disabled" selected="selected" class="please-choose"><?php echo __('&mdash; Select &mdash;') ?></option>
	<?php foreach ($field->getListValuesAsArray() as $key => $title): ?>
		<option value="<?php echo htmlspecialchars($key, ENT_COMPAT | ENT_HTML401, 'UTF-8') ?>">
			<?php echo $title ?>
		</option>
	<?php endforeach ?>
	</select>
<?php else: ?>
	<input
		type="<?php echo $type->getHtmlType() ?>"
		<?php if($type instanceof WpTestingFields_Model_FieldType_Number): ?>min="<?php $type->getMinimum() ?>" step="<?php $type->getStep() ?>"<?php endif ?>
		<?php echo $attributesHTML ?>
		<?php if ($isCheckBox): ?>value="1"<?php endif ?>
		class="wpt__fields_input" />
<?php endif ?>
<?php $inputHTML = ob_get_clean() ?>

<p class="wpt__fields_field wpt__fields_field_<?php echo $field->getType() ?> wpt__fields_field_<?php echo $field->getSlug() ?> wpt__fields_field_<?php echo $f ?>">
	<label class="wpt__fields_title" for="wpt__fields_<?php echo $f ?>">
		<?php if ($isRenderInputInsideLabel): ?><?php echo $inputHTML ?><?php endif ?>
		<?php echo $field->getTitle() ?>
	<?php $wp->doAction('wp_testing_template_fill_form_label_end', array('required' => $field->getIsRequired())) ?>
	</label>
	<input type="hidden" name="<?php echo $fieldValueOrmPrefix ?>field_value_id[<?php echo $f ?>]" value="" />
	<input type="hidden" name="<?php echo $fieldValueOrmPrefix ?>field_id[<?php echo $f ?>]"       value="<?php echo $field->getId() ?>" />
	<?php if ($isRenderInputAfterLabel): ?><?php echo $inputHTML ?><?php endif ?>
	<span class="wpt__fields_clarification comment-notes"><?php echo $field->getClarification() ?></span>
</p>

<?php endforeach ?>
</div>
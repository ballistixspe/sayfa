<?php
/* @var $required boolean */
?>
<?php if ($required === true): ?>
<span class="required">*</span>
<?php elseif ($required === false): ?>
<span class="optional comment-notes">(<?php echo __('optional', 'wp-testing-fields') ?>)</span>
<?php endif ?>
<?php
class WpTestingFields_Query_Field extends WpTesting_Query_AbstractQuery
{
	/**
	 * @return self
	 */
	public static function create($className = __CLASS__)
	{
		return parent::create($className);
	}

	/**
	 * @param fRecordSet $passings
	 * @return WpTesting_Model_Field[]
	 */
	public function findAllByPassings(fRecordSet $passings)
	{
		$testIds = array();
		/* @var $passing WpTesting_Model_Passing */
		foreach ($passings as $passing) {
			$testIds[] = $passing->getTestId();
		}
		$testIds = array_unique($testIds);
		return fRecordSet::build($this->modelName, array(
			'test_id=' => $testIds,
		));
	}
}
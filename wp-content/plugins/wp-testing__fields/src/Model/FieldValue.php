<?php

/**
 * @method integer getId() getId() Gets the current value of id
 * @method string setValue($value) setValue($value) Sets the current value of value
 * @method integer getFieldId() getFieldId() Gets the current value of field id
 */
class WpTestingFields_Model_FieldValue extends WpTesting_Model_AbstractModel
{

	protected $columnAliases = array(
		'id'    => 'field_value_id',
		'value' => 'field_value',
	);

	/**
	 * @return WpTestingFields_Model_Field
	 */
	public function createField()
	{
		return $this->createRelated('WpTestingFields_Model_Field');
	}

	public function setField(WpTestingFields_Model_Field $field)
	{
		return $this->set('field_id', $field->getId());
	}

	/**
	 * @return WpTestingFields_Model_FormulaVariable_FieldValue[]
	 */
	public function extractFormulaVariables()
	{
		$field = $this->createField();
		return $field->createType()->setField($field)->extractFormulaVariables($this->getValue());
	}

	public function getValue()
	{
		$value = $this->get('field_value');

		return empty($value) ? '' : $value;
	}

	public function validateValue(WpTestingFields_Model_FieldValue $me, &$values, &$oldValues, &$relatedRecords, &$cache, &$validationMessages)
	{
		$field = $me->createField();
		$value = trim($values['field_value']);
		if ($value === '') {
			if ($field->getIsRequired()) {
				$validationMessages['field_value_required'] = __('Value is required', 'wp-testing-fields');
			}
			return;
		}
		$field->createType()->setField($field)->validateValue($value, $validationMessages);
	}

	public function getValidationName()
	{
		return $this->createField()->getTitle();
	}

	/**
	 * When field's type allows list, we get value from list's items labels by key
	 * @return string
	 */
	public function getValueAsLabels()
	{
		$field = $this->createField();
		$value = $this->getValue();
		if (!$field->createType()->isAllowList()) {
			return $value;
		}
		$list = $field->getListValuesAsArray();
		if (!isset($list[$value])) {
			return $value;
		}
		return $list[$value];
	}

	protected function configure()
	{
		parent::configure();
		fORM::registerHookCallback($this, 'post::validate()', array($this, 'validateValue'));
	}
}

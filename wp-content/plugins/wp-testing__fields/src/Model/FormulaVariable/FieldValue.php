<?php

class WpTestingFields_Model_FormulaVariable_FieldValue extends WpTesting_Model_FormulaVariable
{

	/**
	 * @var WpTestingFields_Model_Field
	 */
	private $field;

	/**
	 * @var string
	 */
	private $subTitle;

	private static $typeLabel = '';

	public function __construct(WpTestingFields_Model_Field $field, $possibleValueLabel, $variableValue = 0, array $sourceParts = array(), $subTitle = '')
	{
		$this->field    = $field;
		$this->subTitle = $subTitle;

		$source = array_merge(
			array('field', $field->getType()),
			explode('-', $field->getSlug()),
			$sourceParts
		);
		$this
			->setTypeLabel(sprintf(self::$typeLabel, $possibleValueLabel))
			->setSource(implode('_', $source))
			->setValue($variableValue)
		;
	}

	public static function buildAllFrom(WpTestingFields_Model_Test $test, WpTestingFields_Model_Passing $passing)
	{
		self::$typeLabel = __('Field Variable with possible values: %s', 'wp-testing-fields');

		$variables = array();

		/* @var $values WpTestingFields_Model_FieldValue[] */
		foreach ($passing->buildValuesMergedWithTestValues($test) as $value) {
			foreach ($value->extractFormulaVariables() as $variable) {
				$variables[$variable->getSource()] = $variable;
			}
		}

		return $variables;
	}

	public function getTitle()
	{
		$title = $this->field->getTitle();
		if ($this->subTitle) {
			return sprintf('%s, %s', $title, $this->subTitle);
		} else {
			return $title;
		}
	}
}

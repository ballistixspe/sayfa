<?php

class WpTestingFields_Model_FieldType_Date extends WpTestingFields_Model_FieldType
{

	protected static $dateInFormat;
	protected static $yearInFormat;
	protected static $year;

	public function __construct()
	{
		parent::__construct();
		self::$dateInFormat = __('date in format YYYYMMDD', 'wp-testing-fields');
		self::$yearInFormat = __('year of date in format YYYY', 'wp-testing-fields');
		self::$year         = __('year', 'wp-testing-fields');
	}

	public function getKey()
	{
		return 'date';
	}

	public function getLabel()
	{
		return __('Date', 'wp-testing-fields');
	}

	public function getHtmlType()
	{
		return 'date';
	}

	public function extractFormulaVariables($value = null)
	{
		if (empty($value)) {
			$value = '0001-01-01';
		}
		$date = new DateTime($value);

		return array(new WpTestingFields_Model_FormulaVariable_FieldValue(
			$this->field, self::$dateInFormat, $date->format('Ymd')
		), new WpTestingFields_Model_FormulaVariable_FieldValue(
			$this->field, self::$yearInFormat, $date->format('Y'), array('year'), self::$year
		));
	}

	public function validateValue($value, &$validationMessages)
	{
		if (false === filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^\d{4}\-\d{2}\-\d{2}$/')))) {
			$validationMessages['date_invalid'] = sprintf(__('Value "%s" is not a date', 'wp-testing-fields'), $value);
		}
	}
}

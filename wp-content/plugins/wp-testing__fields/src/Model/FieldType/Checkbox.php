<?php

class WpTestingFields_Model_FieldType_Checkbox extends WpTestingFields_Model_FieldType
{

	public function getKey()
	{
		return 'checkbox';
	}

	public function getLabel()
	{
		return __('Checkbox', 'wp-testing-fields');
	}

	public function getHtmlType()
	{
		return 'checkbox';
	}

	public function extractFormulaVariables($value = null)
	{
		return array(new WpTestingFields_Model_FormulaVariable_FieldValue(
			$this->field, self::$zeroOrOne, $value
		));
	}

	public function validateValue($value, &$validationMessages)
	{
		if (!in_array($value, array(0, 1))) {
			$validationMessages['checkbox_invalid'] = sprintf(__('Value "%s" is not a valid checkbox value', 'wp-testing-fields'), $value);
		}
	}
}

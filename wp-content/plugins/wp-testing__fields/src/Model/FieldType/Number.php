<?php

class WpTestingFields_Model_FieldType_Number extends WpTestingFields_Model_FieldType
{
	protected static $anyNumber;

	public function __construct()
	{
		parent::__construct();
		self::$anyNumber = __('any number', 'wp-testing-fields');
	}

	public function getKey()
	{
		return 'number';
	}

	public function getLabel()
	{
		return __('Number', 'wp-testing-fields');
	}

	public function getHtmlType()
	{
		return 'number';
	}

	public function extractFormulaVariables($value = null)
	{
		return array(new WpTestingFields_Model_FormulaVariable_FieldValue(
			$this->field, self::$anyNumber, $value
		));
	}

	public function getMinimum()
	{
		return 1;
	}

	public function getStep()
	{
		return 1;
	}

	public function validateValue($value, &$validationMessages)
	{
		if (false === filter_var($value, FILTER_VALIDATE_FLOAT)) {
			$validationMessages['number_invalid'] = sprintf(__('Value "%s" is not a number', 'wp-testing-fields'), $value);
		}
		if ($value < $this->getMinimum()) {
			$validationMessages['number_less_than_min'] = sprintf(__('Value "%s" is less than mininimum "%d"', 'wp-testing-fields'), $value, $this->getMinimum());
		}
	}
}

<?php

class WpTestingFields_Model_FieldType_Email extends WpTestingFields_Model_FieldType_Text
{

	public function getKey()
	{
		return 'email';
	}

	public function getLabel()
	{
		return __('Email', 'wp-testing-fields');
	}

	public function getDefaultFieldTitle()
	{
		return $this->getLabel();
	}

	public function getHtmlType()
	{
		return 'email';
	}

	public function validateValue($value, &$validationMessages)
	{
		if (false === filter_var($value, FILTER_VALIDATE_EMAIL)) {
			$validationMessages['email_invalid'] = sprintf(__('Value "%s" is not email', 'wp-testing-fields'), $value);
		}
	}
}

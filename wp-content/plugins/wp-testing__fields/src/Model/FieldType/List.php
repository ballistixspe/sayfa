<?php

abstract class WpTestingFields_Model_FieldType_List extends WpTestingFields_Model_FieldType
{
	protected static $selectedValueText;

	public function __construct()
	{
		parent::__construct();
		self::$selectedValueText = __('selected value text', 'wp-testing-fields');
	}

	public function isAllowList()
	{
		return true;
	}

	public function extractFormulaVariables($value = null)
	{
		$variables = array();

		foreach ($this->field->getListValuesAsArray() as $listKey => $listLabel) {
			$variables[] = new WpTestingFields_Model_FormulaVariable_FieldValue(
				$this->field,
				self::$zeroOrOne,
				($listKey == $value),
				explode('-', $listKey),
				$listLabel
			);
		}

		$values = $this->field->getListValuesAsArray();
		$variables[] = new WpTestingFields_Model_FormulaVariable_FieldValue(
			$this->field,
			self::$selectedValueText,
			isset($values[$value]) ? $values[$value] : null
		);

		return $variables;
	}

	protected function getFieldListValuesKeys()
	{
		return array_keys($this->field->getListValuesAsArray());
	}
}

<?php

class WpTestingFields_Model_FieldType_Text extends WpTestingFields_Model_FieldType
{
	protected static $anyText;

	public function __construct()
	{
		parent::__construct();
		self::$anyText = __('any text', 'wp-testing-fields');
	}

	public function getKey()
	{
		return 'text';
	}

	public function getLabel()
	{
		return __('Text', 'wp-testing-fields');
	}

	public function getHtmlType()
	{
		return 'text';
	}

	public function extractFormulaVariables($value = null)
	{
		return array(new WpTestingFields_Model_FormulaVariable_FieldValue(
			$this->field, self::$anyText, $value
		));
	}
}

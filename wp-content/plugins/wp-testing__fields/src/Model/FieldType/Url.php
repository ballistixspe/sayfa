<?php

class WpTestingFields_Model_FieldType_Url extends WpTestingFields_Model_FieldType_Text
{

	public function getKey()
	{
		return 'url';
	}

	public function getLabel()
	{
		return __('URL', 'wp-testing-fields');
	}

	public function getHtmlType()
	{
		return 'url';
	}

	public function validateValue($value, &$validationMessages)
	{
		if (false === filter_var($value, FILTER_VALIDATE_URL)) {
			$validationMessages['url_invalid'] = sprintf(__('Value "%s" is not url', 'wp-testing-fields'), $value);
		}
	}
}

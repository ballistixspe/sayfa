<?php

class WpTestingFields_Model_FieldType_Dropdown extends WpTestingFields_Model_FieldType_List
{

	public function getKey()
	{
		return 'dropdown';
	}

	public function getLabel()
	{
		return __('Dropdown', 'wp-testing-fields');
	}

	public function getHtmlType()
	{
		return '';
	}

	public function getHtmlTag()
	{
		return 'select';
	}

	public function validateValue($value, &$validationMessages)
	{
		$allowedValues = $this->getFieldListValuesKeys();
		if (!in_array($value, $allowedValues)) {
			$validationMessages['dropdown_invalid'] = sprintf(__('Value "%s" is not one of allowed: %s', 'wp-testing-fields'), $value, implode(', ', $allowedValues));
		}
	}
}

<?php

class WpTestingFields_Model_Test extends WpTesting_Model_Test
{

	/**
	 * @return WpTestingFields_Model_Field[]
	 */
	public function buildFields()
	{
		return $this->me()->buildRelated('WpTestingFields_Model_Fields');
	}

	/**
	 * @return WpTestingFields_Model_FieldValue[]
	 */
	public function buildValuesFromFields()
	{
		$values = array();
		foreach ($this->buildFields() as $field) {
			$value    = new WpTestingFields_Model_FieldValue();
			$values[] = $value->setField($field);
		}
		return fRecordSet::buildFromArray('WpTestingFields_Model_FieldValue', $values);
	}

	public function isDenoteRequiredFields()
	{
		return $this->me()->isOptionEnabled('wpt_test_page_denote_required_fields');
	}

	public function isDenoteOptionalFields()
	{
		return $this->me()->isOptionEnabled('wpt_test_page_denote_optional_fields', 1);
	}

	public function isPlaceFieldsBeforeQuestions()
	{
		return $this->me()->isOptionEqual('wpt_test_page_place_fields', 'before_questions', 'before_questions');
	}

	public function isPlaceFieldsAfterQuestions()
	{
		return $this->me()->isOptionEqual('wpt_test_page_place_fields', 'after_questions');
	}

	/**
	 * @param array $variables
	 * @param WpTestingFields_Model_Passing $passing
	 * @return WpTesting_Model_FormulaVariable[]
	 */
	public function buildPublicFormulaVariables(WpTesting_Model_Passing $passing = null)
	{
		$variables = array();

		$variables += WpTestingFields_Model_FormulaVariable_FieldValue::buildAllFrom($this, $passing);

		return $variables;
	}

	public function adaptForPopulate($request, $testId)
	{
		$fieldPrefix  = fORMRelated::determineRequestFilter('WpTesting_Model_Test', 'WpTestingFields_Model_Field', 'test_id');
		$request     += array(
			'wpt__field_id' => array(),
		);

		$fields = json_decode(stripslashes($request['wpt__fields_json']), /* assoc */ true);
		foreach ($fields as $field) {
			$request[$fieldPrefix . 'test_id'] [] = $testId;
			foreach ($field as $key => $value) {
				if ($key == 'is_required') {
					$value = (int)$value;
				}
				if ($key == 'list_values') {
					$value = preg_split('/\s*[\r\n]+\s*/', $value);
					$value = array_filter($value);
					$value = implode(PHP_EOL, $value);
				}
				$request[$fieldPrefix . 'field_' . $key][] = $value;
			}
		}

		return $request;
	}

	public function populateAll()
	{
		return $this->populateFields();
	}

	/**
	 * @param bool $isRecursive
	 * @return WpTestingFields_Model_Test
	 */
	public function populateFields($isRecursive = false)
	{
		$this->me()->populateRelated('WpTestingFields_Model_Fields', $isRecursive);

		return $this;
	}
}
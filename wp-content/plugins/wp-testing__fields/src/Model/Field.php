<?php

/**
 * @method integer getId() getId() Gets the current value of id
 * @method integer getTestId() getTestId() Gets the current value of test id
 * @method string getTitle() getTitle() Gets the current value of title
 * @method string getType() getType() Gets the current value of type
 * @method boolean getIsRequired() getIsRequired() Gets the current value of is required
 * @method integer getSort() getSort() Gets the current value of sort
 * @method string getClarification() getClarification() Gets the current value of clarification
 * @method string getListValues() getListValues() Gets the current value of list values
 * @method string getDefaultValue() getDefaultValue() Gets the current value of default value
 * @method WpTestingFields_Model_Field setId() setId(integer $id) Sets the value for id
 * @method WpTestingFields_Model_Field setTestId() setTestId(integer $test_id) Sets the value for test id
 * @method WpTestingFields_Model_Field setTitle() setTitle(string $title) Sets the value for title
 * @method WpTestingFields_Model_Field setType() setType(string $type) Sets the value for type
 * @method WpTestingFields_Model_Field setIsRequired() setIsRequired(boolean $is_required) Sets the value for is required
 * @method WpTestingFields_Model_Field setSort() setSort(integer $sort) Sets the value for sort
 * @method WpTestingFields_Model_Field setClarification() setClarification(string $clarification) Sets the value for clarification
 * @method WpTestingFields_Model_Field setListValues() setListValues(string $list_values) Sets the value for list values
 * @method WpTestingFields_Model_Field setDefaultValue() setDefaultValue(string $default_value) Sets the value for default value
 */
class WpTestingFields_Model_Field extends WpTesting_Model_AbstractModel implements JsonSerializable
{
	protected $columnAliases = array(
		'id'			=> 'field_id',
		'title'			=> 'field_title',
		'type'			=> 'field_type',
		'is_required'	=> 'field_is_required',
		'sort'			=> 'field_sort',
		'clarification'	=> 'field_clarification',
		'list_values'	=> 'field_list_values',
		'default_value'	=> 'field_default_value',
	);

	/**
	 * @var WpTestingFields_Model_FieldType
	 */
	private $fieldType = null;

	/**
	 * @var string
	 */
	private $slug = null;

	public function getSlug()
	{
		if (is_null($this->slug)) {
			$this->slug = $this->getWp()->sanitizeTitle($this->getTitle(), 'id-' . $this->getId());
			$this->slug = str_replace(' ', '-', urldecode($this->slug));
		}
		return $this->slug;
	}

	/**
	 * @return WpTestingFields_Model_FieldType
	 */
	public function createType()
	{
		if (!is_null($this->fieldType)) {
			return $this->fieldType;
		}

		$mapping   = $this->getWp()->applyFilters('wp_testing_fields_field_type_mapping', array(
			'text'    => 'WpTestingFields_Model_FieldType_Text',
			'number'  => 'WpTestingFields_Model_FieldType_Number',
			'email'   => 'WpTestingFields_Model_FieldType_Email',
			'dropdown'=> 'WpTestingFields_Model_FieldType_Dropdown',
			'checkbox'=> 'WpTestingFields_Model_FieldType_Checkbox',
			'url'     => 'WpTestingFields_Model_FieldType_Url',
			'date'    => 'WpTestingFields_Model_FieldType_Date',
		));

		$type = $this->getType();
		if (isset($mapping[$type]) && class_exists($mapping[$type])) {
			$className = $mapping[$type];
		} else {
			$className = 'WpTestingFields_Model_FieldType_Text';
		}
		$this->fieldType = new $className();

		return $this->fieldType;
	}

	public function hasValues()
	{
		return fRecordSet::build(
			$model = 'WpTestingFields_Model_FieldValue',
			$where = array('field_id=' => $this->getId()),
			$order = array(),
			$limit = 1
		)->count() > 0;
	}

	public function getListValuesAsArray()
	{
		$values = array();
		foreach (explode(PHP_EOL, $this->getListValues()) as $key => $value) {
			$key = urldecode($this->wp->sanitizeTitle($value, 'value-' . $key));
			$values[$key] = $value;
		}
		return $values;
	}

	public function jsonSerialize() {
		return array(
			'id'                 => $this->getId(),
			'type'               => $this->getType(),
			'title'              => $this->getTitle(),
			'has_values'         => $this->hasValues(),
			'is_required'        => $this->getIsRequired(),
			'clarification'      => $this->getClarification(),
			'list_values'        => $this->getListValues(),
			'type_full_label'    => $this->createType()->getFullLabel(),
			'type_allow_list'    => $this->createType()->isAllowList(),
		);
	}
}

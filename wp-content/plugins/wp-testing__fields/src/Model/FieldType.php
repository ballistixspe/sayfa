<?php

abstract class WpTestingFields_Model_FieldType implements JsonSerializable
{
	/**
	 * @var WpTestingFields_Model_Field
	 */
	protected $field;

	abstract public function getKey();
	abstract public function getLabel();

	protected static $zeroOrOne;

	public function __construct()
	{
		self::$zeroOrOne = __('0 or 1', 'wp-testing-fields');
	}

	/**
	 * The value for HTML input type
	 * @return string
	 */
	abstract public function getHtmlType();

	public function getHtmlTag()
	{
		return 'input';
	}

	public function isAllowList()
	{
		return false;
	}

	/**
	 * @return WpTestingFields_Model_FormulaVariable_FieldValue[]
	 */
	public function extractFormulaVariables($value = null)
	{
		return array();
	}

	public function getFullLabel()
	{
		return sprintf(__('%s Field', 'wp-testing-fields'), $this->getLabel());
	}

	public function getDefaultFieldTitle()
	{
		return '';
	}

	public function jsonSerialize()
	{
		return array(
			'key'                => $this->getKey(),
			'label'              => $this->getLabel(),
			'fullLabel'          => $this->getFullLabel(),
			'defaultFieldTitle'  => $this->getDefaultFieldTitle(),
			'allowList'          => $this->isAllowList(),
		);
	}

	public function setField(WpTestingFields_Model_Field $field)
	{
		$this->field = $field;
		return $this;
	}

	public function validateValue($value, &$validationMessages)
	{
	}
}

<?php

class WpTestingFields_Model_Passing extends WpTesting_Model_Passing
{

	public function populateAll()
	{
		return $this->populateFieldValues();
	}

	public function getId()
	{
		return $this->me()->get('id');
	}

	/**
	 * @return WpTestingFields_Model_FieldValue[]
	 */
	public function buildValues()
	{
		return $this->me()->buildRelated('WpTestingFields_Model_FieldValues');
	}

	/**
	 * Usable in case we have old passing with updated test with new values that used in formulas.
	 *
	 * New values should be supplied as null.
	 *
	 * @return WpTestingFields_Model_FieldValue[]
	 */
	public function buildValuesMergedWithTestValues(WpTestingFields_Model_Test $test)
	{
		$testValues = $test->buildValuesFromFields();

		if (!$this->getId()) {
			return $testValues;
		}

		$passingValues = $this->me()->buildRelated('WpTestingFields_Model_FieldValues');

		if (count($passingValues) >= count($testValues)) {
			return $passingValues;
		}

		$testValuesByFieldId = array();
		/* @var $value WpTestingFields_Model_FieldValue */
		foreach ($testValues as $i => $value) {
			$testValuesByFieldId[$value->getFieldId()] = $value;
		}

		/* @var $value WpTestingFields_Model_FieldValue */
		foreach ($passingValues as $i => $value) {
			unset($testValuesByFieldId[$value->getFieldId()]);
		}

		return $passingValues->merge($testValuesByFieldId);
	}

	/**
	 * @return array
	 */
	public function getFieldValuesBySlug($slug)
	{
		$values = array();
		foreach ($this->buildValues() as $value) {
			if ($value->createField()->getSlug() == $slug) {
				$values[] = $value->getValueAsLabels();
			}
		}
		return $values;
	}

	/**
	 * @param bool $isRecursive
	 * @return self
	 */
	public function populateFieldValues($isRecursive = false)
	{
		$this->me()->populateRelated('WpTestingFields_Model_FieldValues', $isRecursive);
		return $this;
	}

	protected function configure()
	{
		fORMRelated::registerValidationNameMethod('WpTestingFields_Model_Passing', 'WpTestingFields_Model_FieldValue', 'getValidationName');
		fORMRelated::registerValidationNameMethod('WpTesting_Model_Passing',       'WpTestingFields_Model_FieldValue', 'getValidationName');
	}
}
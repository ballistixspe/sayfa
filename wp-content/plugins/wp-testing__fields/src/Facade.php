<?php

class WpTestingFields_Facade implements WpTesting_Addon_IAddon
{

	/**
	 * @var WpTesting_Addon_IWordPressFacade
	 */
	private $wp;

	/**
	 * @var WpTestingFields_Model_Test
	 */
	private $test = null;

	/**
	 * @var WpTestingFields_Model_Passing
	 */
	private $passing = null;

	public function __construct(WpTesting_Addon_IFacade $plugin)
	{
		$plugin->registerAddon($this);
		$this->registerHooks();
	}

	public function getClass()
	{
		return get_class($this);
	}

	public function getRoot()
	{
		return dirname(__FILE__);
	}

	public function setWp(WpTesting_Addon_IWordPressFacade $wp)
	{
		$this->wp = $wp;
		return $this;
	}

	public function setupORM(fSchema $schema, fDatabase $database, WpTesting_Facade_IORM $ormAware)
	{
	    $wpPrefix  = $this->wp->getTablePrefix();
        $wptPrefix = $ormAware->getTablePrefix();

		fORM::mapClassToTable('WpTestingFields_Model_Test',       $wpPrefix  . 'posts');
		fORM::mapClassToTable('WpTestingFields_Model_Field',      $wptPrefix . 'fields');
		fORM::mapClassToTable('WpTestingFields_Model_FieldValue', $wptPrefix . 'field_values');
		fORM::mapClassToTable('WpTestingFields_Model_Passing',    $wptPrefix . 'passings');

		$schema->setKeysOverride(array(
			array(
				'column'         => 'test_id',
				'foreign_table'  => $wpPrefix . 'posts',
				'foreign_column' => 'ID',
				'on_delete'      => 'cascade',
				'on_update'      => 'cascade',
			),
		), $wptPrefix . 'fields', 'foreign');

		$schema->setKeysOverride(array(
			array(
				'column'         => 'field_id',
				'foreign_table'  => $wptPrefix . 'fields',
				'foreign_column' => 'field_id',
				'on_delete'      => 'restrict',
				'on_update'      => 'cascade',
			),
			array(
				'column'         => 'passing_id',
				'foreign_table'  => $wptPrefix . 'passings',
				'foreign_column' => 'passing_id',
				'on_delete'      => 'cascade',
				'on_update'      => 'cascade',
			),
		), $wptPrefix . 'field_values', 'foreign');
	}

	public function editorCustomizeUi()
	{
		$editor = new WpTestingFields_Doer_Editor($this->wp);
		$editor->customizeUi();
	}

	public function testAdaptForPopulate($request, $testId, WpTesting_Model_Test $parentTest)
	{
		return $this->createTestOnce($parentTest)->adaptForPopulate($request, $testId);
	}

	public function testPopulateAll(WpTesting_Model_Test $parentTest)
	{
		return $this->createTestOnce($parentTest)->populateAll();
	}

	public function testBuildPublicVariables($variables, WpTesting_Model_Test $parentTest, WpTesting_Model_Passing $parentPassing = null)
	{
	    return $variables + $this->createTestOnce($parentTest)->buildPublicFormulaVariables($this->createPassingOnce($parentPassing));
	}

	public function passerFillFormRenderFields(WpTesting_Model_Passing $parentPassing, WpTesting_Model_Test $parentTest)
	{
		$passer = new WpTestingFields_Doer_Passer($this->wp);
		$passer->setupFillForm($parentPassing, $this->createTestOnce($parentTest));
	}

	public function passingPopulateAll(WpTesting_Model_Passing $parentPassing)
	{
		return $this->createPassingOnce($parentPassing)->populateAll();
	}

	public function passingTableAddFields(WpTesting_Widget_PassingTable $table)
	{
		$browser = new WpTestingFields_Doer_PassingBrowser($this->wp);
		$browser->renderAdminPassingsPage($table);
	}

	private function createTestOnce(WpTesting_Model_Test $parentTest)
	{
		if (is_null($this->test) || !$parentTest->isRelative($this->test)) {
			$this->test = new WpTestingFields_Model_Test($parentTest);
		}
		return $this->test;
	}

	private function createPassingOnce(WpTesting_Model_Passing $parentPassing = null)
	{
		if (is_null($this->passing) || is_null($parentPassing) || !$parentPassing->isRelative($this->passing)) {
			$this->passing = new WpTestingFields_Model_Passing($parentPassing);
		}
		return $this->passing;
	}

	private function registerHooks()
	{
	    $defaultPriority = WpTesting_Addon_IWordPressFacade::PRIORITY_DEFAULT;
		$this->wp
			->addAction('wp_testing_orm_setup',
		        array($this, 'setupORM'), $defaultPriority, 3)
			->addAction('wp_testing_editor_customize_ui_before_angular_run',
		        array($this, 'editorCustomizeUi'))
			->addFilter('wp_testing_test_adapt_for_populate',
		        array($this, 'testAdaptForPopulate'), $defaultPriority, 3)
			->addAction('wp_testing_test_populate_all_after',
		        array($this, 'testPopulateAll'))
			->addFilter('wp_testing_test_build_public_formula_variables',
		        array($this, 'testBuildPublicVariables'), $defaultPriority, 3)
			->addAction('wp_testing_passer_fill_form_before_render',
		        array($this, 'passerFillFormRenderFields'), $defaultPriority, 2)
			->addFilter('wp_testing_passing_populate_all_after',
		        array($this, 'passingPopulateAll'), $defaultPriority, 2)
			->addAction('wp_testing_passing_browser_create_table',
		        array($this, 'passingTableAddFields'))
		;
		return $this;
	}
}
		   	 		  			 	 	   		      	 				     		 		  	 		 	 	   		  	 	 		 			    		    	 		   					  	 	 	 	 			  	 	 	 				   			  	   	 	 		 	 	  		     	 	
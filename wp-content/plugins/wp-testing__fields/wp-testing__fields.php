<?php
/**
 * Plugin Name: Wp-testing — Fields
 * Plugin URI: http://apsiholog.ru/psychological-tests/
 * Description: Add custom form fields like name, email, sex and etc. Use their values in formulas and see in respondents' results. Denote required/optional fields, place fields before or after questions.
 * Version: 0.6.7
 * Author: Alexander Ustimenko
 * Author URI: http://ustimen.co
 * License: CC BY-NC-ND 4.0
 * Text Domain: wp-testing-fields
 * Domain Path: /languages
 */

if (isset($WpTesting_Facade)) {
	require_once dirname(__FILE__) . '/src/Facade.php';
	new WpTestingFields_Facade($WpTesting_Facade);
}
